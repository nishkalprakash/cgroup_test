echo (){ 
    real_echo=/usr/bin/echo; $real_echo -n $(date) " "; $real_echo $@; 
}
res="privileged/cgroup1"
# echo "Running for $res"
echo "Running CS1 - $res"
cd /home/darkskull/DarKSkuLL/cgroup_test/case_study_I/docker_20.10/cgroup1/
bash auto_cs1.sh $res
sleep 1800

echo "Running CS2 - $res"
cd /home/darkskull/DarKSkuLL/cgroup_test/case_study_II/docker_20.10/cgroup1/
bash auto_cs2.sh $res
sleep 1800

echo "Running CS3 - $res"
cd /home/darkskull/DarKSkuLL/cgroup_test/case_study_III/docker_20.10/
bash auto_cs3.sh $res
sleep 1800

echo "Running CS4 - $res"
cd /home/darkskull/DarKSkuLL/cgroup_test/case_study_IV/docker_20.10/
bash auto_cs4.sh $res
sleep 1800

echo "Running CS5 - $res"
cd /home/darkskull/DarKSkuLL/cgroup_test/case_study_V/docker_20.10/
bash auto_cs5.sh $res
