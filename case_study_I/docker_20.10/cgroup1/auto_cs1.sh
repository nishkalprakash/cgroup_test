#!/bin/bash

# echo "for linux"
# res="results"
# echo "for windows"
# res="results/win"
echo (){
    real_echo=/usr/bin/echo; $real_echo -n $(date) " "; $real_echo $@;
}

if [ $(uname) == 'Linux' ]
then
    if [ -z $1 ]
    then
        res='results/ubuntu'
    else
        res="results/ubuntu/$1"
    fi
else
    res='results/win'
fi
mkdir -p $res
tag="cs1"
##################################
echo "System Info"
bash system_info.sh $res
#################################
##################################
echo "Setup Images"
bash setup_images.sh $res $tag
#################################

# START For 3_dos

echo "START For 3_dos"
base="$res/3_dos"
mkdir -p $base

out="$base/sysbench"
mkdir -p $out
###########
# for loop in 'exception_loop'
for loop in {'idle_loop','exception_loop'}
do
    echo "Victim running sysbench with attacker running $loop"
    att=$(docker run -d \
            --cpuset-cpus "0-0" \
            --cpu-period 200000 \
            --cpu-quota 200000 \
            attacker:$tag bash $loop.sh $base/$loop/same_core.txt)
    # for core in '2-2'
    for core in {'0-0','2-2'}
    do
        sys_out=$out/"$loop"_$core.txt
        docker run -it \
            --cpuset-cpus "$core" \
            --cpu-period 200000 \
            --cpu-quota 200000 \
            --mount src="$(pwd)/results",dst="/home/results",type=bind \
            victim:$tag bash sysbench_test.sh "$sys_out"
        
        python extract_sysbench.py "$sys_out"
    done
    docker stop $att

done
###########

echo "Removing all the containers "

docker stop $(docker ps -q)
docker rm $(docker ps -aq)

echo "END For 2_dos"
# END For 2_dos

################################
