if [ -z "$1" ]
then 
    di="results/ubuntu/docker_info"
else
    di="$1/docker_info"
fi
mkdir -p $di

if [ -z "$2" ]
then
    tag='cs3'
else 
    tag=$2
fi

docker info > $di/docker_info.txt 2>&1
## Building the base image
for i in $(ls images)
do
    docker build "images/$i" \
        --tag $i:$tag
    echo "Built image $i:$tag"
    docker inspect $i:$tag > $di/"docker_inspect_$i"_$tag.txt 2>&1
done

# ## Running the images as different named containers

# docker run -it -d \
#     --name "victim" \
#     --mount src="$(pwd)/$tag",dst="/home/$tag",type=bind \
#     --cpuset-cpus "0-0" \
#     victim:$tag

# docker run -it -d \
#     --name "attacker1" \
#     --mount src="$(pwd)/$tag",dst="/home/$tag",type=bind \
#     --cpuset-cpus "1-1" \
#     victim:$tag

# docker run -it -d \
#     --name "attacker" \
#     --mount src="$(pwd)/$tag",dst="/home/$tag",type=bind \
#     --cpuset-cpus "0-0" \
#     victim:$tag

# ## To stop all the containers
# docker stop `docker ps -q`
