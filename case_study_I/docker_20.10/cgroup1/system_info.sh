
if [ $(uname) == 'Linux' ]
then
    if [ -z "$1" ]
    then 
        res="results/ubuntu/host_info"
    else
        res="$1/host_info"
    fi
    mkdir -p $res

    # res='results/ubuntu'
    bash neofetch > "$res/neofetch.txt"
    uname -a > "$res/uname_a.txt"
    lshw --html > "$res/lshw.html"
    lshw  > "$res/lshw.txt"
else
    if [ -z "$1" ]
    then 
        res="results/win/host_info"
    else
        res="$1/host_info"
    fi
    mkdir -p $res

    # res='results/ubuntu'
    uname -a > "$res/uname_a.txt"
    systeminfo > "$res/systeminfo.txt"
fi
