# From official ubuntu image
FROM ubuntu:16.04

# updating and installing tests
#RUN apt-get update 
#RUN apt-get install -y git fio sysbench gcc
RUN apt-get update 

## For editing files inside live container
# RUN apt-get install -y vim

## For sysbench tests
# RUN apt-get install -y sysbench 

## for stats
# RUN apt-get install -y sysstat

## For FIO TESTS
# RUN apt-get install -y fio 

# ## Modules required for unixbench
# RUN apt-get install -y libx11-dev \
#     libgl1-mesa-dev \
#     libxext-dev \
#     perl \
#     perl-modules \
#     make \
#     gcc \
#     git

WORKDIR /home/cs1/

# RUN git clone https://github.com/kdlucas/byte-unixbench.git

COPY cs1/ /home/cs1/

# CMD /bin/bash
