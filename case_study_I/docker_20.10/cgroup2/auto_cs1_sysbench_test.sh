#!/bin/bash

## Attacker Idle Loop on different core
loop="idle_loop"
att=$(docker run -d \
    --cpuset-cpus "1-1" \
    --cpu-period 200000 \
    --cpu-quota 200000 \
    attacker:cs1 bash $loop.sh)

out="win_different_core_$loop.txt"
docker run -it \
    --cpuset-cpus "0-0" \
    --cpu-period 200000 \
    --cpu-quota 200000 \
    --mount src="$(pwd)/cs1",dst="/home/cs1",type=bind \
    victim:cs1 bash sysbench_test.sh "$out"

docker stop $att

python extract_sysbench.py "cs1/results/sysbench_results/$out"

## Attacker Exception Loop On different core
loop="exception_loop"
att=$(docker run -d \
    --cpuset-cpus "1-1" \
    --cpu-period 200000 \
    --cpu-quota 200000 \
    attacker:cs1 bash $loop.sh)

out="win_different_core_$loop.txt"
docker run -it \
    --cpuset-cpus "0-0" \
    --cpu-period 200000 \
    --cpu-quota 200000 \
    --mount src="$(pwd)/cs1",dst="/home/cs1",type=bind \
    victim:cs1 bash sysbench_test.sh "$out"

docker stop $att

python extract_sysbench.py "cs1/results/sysbench_results/$out"

## Attacker Exception Loop On same core
loop="exception_loop"
att=$(docker run -d \
    --cpuset-cpus "0-0" \
    --cpu-period 200000 \
    --cpu-quota 200000 \
    attacker:cs1 bash $loop.sh)

out="win_same_core_$loop.txt"
docker run -it \
    --cpuset-cpus "0-0" \
    --cpu-period 200000 \
    --cpu-quota 200000 \
    --mount src="$(pwd)/cs1",dst="/home/cs1",type=bind \
    victim:cs1 bash sysbench_test.sh "$out"

docker stop $att

python extract_sysbench.py "cs1/results/sysbench_results/$out"

## To remove all the containers and vars
unset loop
unset att
docker rm $(docker ps -aq)