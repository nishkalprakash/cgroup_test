#!/bin/bash 
pw=$(pwd)
ubase="$pw/results/sysbench_results"

mkdir -p $ubase

if [ -z "$1" ]
    then 
    results_file= "$ubase/sysbench_results.txt"
else
    results_file="$ubase/$1"
fi
## This is to perform the io writes test inside the container and not in the mount folder
cd /home
for i in {1..1};
do
    ( sysbench --test=cpu run ; \
    sysbench --test=memory run ; \
    sysbench --test=fileio --file-test-mode=seqrd prepare ; \
    sysbench --test=fileio --file-test-mode=seqrd run ; \
    sysbench --test=fileio --file-test-mode=seqrd cleanup ; \
    sysbench --test=fileio --file-test-mode=seqwr prepare ; \
    sysbench --test=fileio --file-test-mode=seqwr run ; \
    sysbench --test=fileio --file-test-mode=seqwr cleanup \
    ) >> "$results_file" 2>&1 ;
    echo "Done for $i";
done
# cd $pw
# python3 extract_sysbench.py $results_file