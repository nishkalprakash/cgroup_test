# docker build . \
#     --no-cache \
#     --tag victim:cs1 \
#     --cpuset-cpus "0-0"

## Building the base image
docker build -f victim.Dockerfile . \
    --tag victim:cs1

docker build -f attacker.Dockerfile . \
    --tag attacker:cs1



## Running the images as different named containers

# docker run -it -d \
#     --name "victim_cs1" \
#     --cpuset-cpus "0-0" \
#     --cpu-period 200000 \
#     --cpu-quota 200000 \
#     --mount src="$(pwd)/cs1",dst="/home/cs1",type=bind \
#     victim:cs1

# docker run -it -d \
#     --name "attacker1_cs1" \
#     --cpuset-cpus "1-1" \
#     --cpu-period 200000 \
#     --cpu-quota 200000 \
#     attacker:cs1
#     #\    --mount src="$(pwd)/cs1",dst="/home/cs1",type=bind \

# docker run -it -d \
#     --name "attacker_cs1" \
#     --cpuset-cpus "0-0" \
#     --cpu-period 200000 \
#     --cpu-quota 200000 \
#     attacker:cs1
#     # --mount src="$(pwd)/cs1",dst="/home/cs1",type=bind \

# ## To stop all the containers
# docker stop `docker ps -q`

## To delete all containers 
# docker stop `docker ps -aq`