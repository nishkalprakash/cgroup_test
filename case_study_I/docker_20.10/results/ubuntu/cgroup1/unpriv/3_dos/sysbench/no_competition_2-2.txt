sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.5895s
    total number of events:              10000
    total time taken by event execution: 13.5882
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.36ms
         max:                                  2.39ms
         approx.  95 percentile:               1.36ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.5882/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2168402.87 ops/sec)

102400.00 MB transferred (2117.58 MB/sec)


Test execution summary:
    total time:                          48.3571s
    total number of events:              104857600
    total time taken by event execution: 38.1150
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  2.26ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.1150/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (4.1035Gb/sec)
268928.93 Requests/sec executed

Test execution summary:
    total time:                          0.4874s
    total number of events:              131072
    total time taken by event execution: 0.4640
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.08ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   0.4640/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (62.38Mb/sec)
 3992.33 Requests/sec executed

Test execution summary:
    total time:                          32.8309s
    total number of events:              131072
    total time taken by event execution: 12.4547
    per-request statistics:
         min:                                  0.01ms
         avg:                                  0.10ms
         max:                                 16.04ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   12.4547/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.5979s
    total number of events:              10000
    total time taken by event execution: 13.5965
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.36ms
         max:                                  2.75ms
         approx.  95 percentile:               1.37ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.5965/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2168391.77 ops/sec)

102400.00 MB transferred (2117.57 MB/sec)


Test execution summary:
    total time:                          48.3573s
    total number of events:              104857600
    total time taken by event execution: 38.1144
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.05ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.1144/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (4.0835Gb/sec)
267619.37 Requests/sec executed

Test execution summary:
    total time:                          0.4898s
    total number of events:              131072
    total time taken by event execution: 0.4661
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  4.09ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   0.4661/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (61.921Mb/sec)
 3962.94 Requests/sec executed

Test execution summary:
    total time:                          33.0745s
    total number of events:              131072
    total time taken by event execution: 12.5399
    per-request statistics:
         min:                                  0.01ms
         avg:                                  0.10ms
         max:                                 15.01ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   12.5399/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.5983s
    total number of events:              10000
    total time taken by event execution: 13.5969
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.36ms
         max:                                  2.75ms
         approx.  95 percentile:               1.37ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.5969/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2167148.50 ops/sec)

102400.00 MB transferred (2116.36 MB/sec)


Test execution summary:
    total time:                          48.3851s
    total number of events:              104857600
    total time taken by event execution: 38.1362
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.09ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.1362/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (4.0898Gb/sec)
268026.72 Requests/sec executed

Test execution summary:
    total time:                          0.4890s
    total number of events:              131072
    total time taken by event execution: 0.4652
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.10ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   0.4652/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (60.818Mb/sec)
 3892.32 Requests/sec executed

Test execution summary:
    total time:                          33.6745s
    total number of events:              131072
    total time taken by event execution: 12.8861
    per-request statistics:
         min:                                  0.01ms
         avg:                                  0.10ms
         max:                                 15.51ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   12.8861/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.5919s
    total number of events:              10000
    total time taken by event execution: 13.5905
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.36ms
         max:                                  2.79ms
         approx.  95 percentile:               1.36ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.5905/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2169582.99 ops/sec)

102400.00 MB transferred (2118.73 MB/sec)


Test execution summary:
    total time:                          48.3308s
    total number of events:              104857600
    total time taken by event execution: 38.0941
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.13ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.0941/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (4.0457Gb/sec)
265137.33 Requests/sec executed

Test execution summary:
    total time:                          0.4944s
    total number of events:              131072
    total time taken by event execution: 0.4701
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.10ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   0.4701/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (60.498Mb/sec)
 3871.88 Requests/sec executed

Test execution summary:
    total time:                          33.8523s
    total number of events:              131072
    total time taken by event execution: 12.7360
    per-request statistics:
         min:                                  0.01ms
         avg:                                  0.10ms
         max:                                342.51ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   12.7360/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.6027s
    total number of events:              10000
    total time taken by event execution: 13.6014
    per-request statistics:
         min:                                  1.36ms
         avg:                                  1.36ms
         max:                                  2.67ms
         approx.  95 percentile:               1.36ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.6014/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2170035.68 ops/sec)

102400.00 MB transferred (2119.18 MB/sec)


Test execution summary:
    total time:                          48.3207s
    total number of events:              104857600
    total time taken by event execution: 38.0856
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.08ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.0856/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (4.055Gb/sec)
265749.20 Requests/sec executed

Test execution summary:
    total time:                          0.4932s
    total number of events:              131072
    total time taken by event execution: 0.4693
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.13ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   0.4693/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (59.364Mb/sec)
 3799.27 Requests/sec executed

Test execution summary:
    total time:                          34.4993s
    total number of events:              131072
    total time taken by event execution: 13.1070
    per-request statistics:
         min:                                  0.01ms
         avg:                                  0.10ms
         max:                                 15.04ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   13.1070/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.6066s
    total number of events:              10000
    total time taken by event execution: 13.6053
    per-request statistics:
         min:                                  1.36ms
         avg:                                  1.36ms
         max:                                  3.62ms
         approx.  95 percentile:               1.36ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.6053/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2169096.85 ops/sec)

102400.00 MB transferred (2118.26 MB/sec)


Test execution summary:
    total time:                          48.3416s
    total number of events:              104857600
    total time taken by event execution: 38.1027
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  2.26ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.1027/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (4.068Gb/sec)
266601.43 Requests/sec executed

Test execution summary:
    total time:                          0.4916s
    total number of events:              131072
    total time taken by event execution: 0.4677
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.09ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   0.4677/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (58.446Mb/sec)
 3740.53 Requests/sec executed

Test execution summary:
    total time:                          35.0410s
    total number of events:              131072
    total time taken by event execution: 13.2689
    per-request statistics:
         min:                                  0.01ms
         avg:                                  0.10ms
         max:                                 15.09ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   13.2689/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
