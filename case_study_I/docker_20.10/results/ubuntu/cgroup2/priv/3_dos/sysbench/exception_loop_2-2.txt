sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          870.8077s
    total number of events:              10000
    total time taken by event execution: 869.6425
    per-request statistics:
         min:                                  1.51ms
         avg:                                 86.96ms
         max:                                805.66ms
         approx.  95 percentile:             281.64ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   869.6425/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (27106.01 ops/sec)

102400.00 MB transferred (26.47 MB/sec)


Test execution summary:
    total time:                          3868.4260s
    total number of events:              104857600
    total time taken by event execution: 3064.5254
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.03ms
         max:                               1405.54ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   3064.5254/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (9.4199Mb/sec)
  602.87 Requests/sec executed

Test execution summary:
    total time:                          217.4117s
    total number of events:              131072
    total time taken by event execution: 217.1704
    per-request statistics:
         min:                                  0.00ms
         avg:                                  1.66ms
         max:                               2326.09ms
         approx.  95 percentile:              11.79ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   217.1704/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (11.848Mb/sec)
  758.26 Requests/sec executed

Test execution summary:
    total time:                          172.8598s
    total number of events:              131072
    total time taken by event execution: 156.0673
    per-request statistics:
         min:                                  0.02ms
         avg:                                  1.19ms
         max:                                660.17ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   156.0673/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          802.8566s
    total number of events:              10000
    total time taken by event execution: 801.8388
    per-request statistics:
         min:                                  1.55ms
         avg:                                 80.18ms
         max:                                729.76ms
         approx.  95 percentile:             249.86ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   801.8388/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (27437.18 ops/sec)

102400.00 MB transferred (26.79 MB/sec)


Test execution summary:
    total time:                          3821.7334s
    total number of events:              104857600
    total time taken by event execution: 3009.6659
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.03ms
         max:                                816.11ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   3009.6659/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (11.074Mb/sec)
  708.74 Requests/sec executed

Test execution summary:
    total time:                          184.9362s
    total number of events:              131072
    total time taken by event execution: 183.7259
    per-request statistics:
         min:                                  0.00ms
         avg:                                  1.40ms
         max:                               1027.82ms
         approx.  95 percentile:               7.79ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   183.7259/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (11.244Mb/sec)
  719.61 Requests/sec executed

Test execution summary:
    total time:                          182.1438s
    total number of events:              131072
    total time taken by event execution: 162.5437
    per-request statistics:
         min:                                  0.02ms
         avg:                                  1.24ms
         max:                                769.01ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   162.5437/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          791.3676s
    total number of events:              10000
    total time taken by event execution: 790.3937
    per-request statistics:
         min:                                  1.51ms
         avg:                                 79.04ms
         max:                               1113.65ms
         approx.  95 percentile:             253.70ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   790.3937/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (26331.82 ops/sec)

102400.00 MB transferred (25.71 MB/sec)


Test execution summary:
    total time:                          3982.1634s
    total number of events:              104857600
    total time taken by event execution: 3145.0566
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.03ms
         max:                                904.04ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   3145.0566/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (7.7717Mb/sec)
  497.39 Requests/sec executed

Test execution summary:
    total time:                          263.5198s
    total number of events:              131072
    total time taken by event execution: 262.6962
    per-request statistics:
         min:                                  0.00ms
         avg:                                  2.00ms
         max:                               3631.58ms
         approx.  95 percentile:              11.80ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   262.6962/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (11.175Mb/sec)
  715.19 Requests/sec executed

Test execution summary:
    total time:                          183.2697s
    total number of events:              131072
    total time taken by event execution: 158.0811
    per-request statistics:
         min:                                  0.02ms
         avg:                                  1.21ms
         max:                                576.16ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   158.0811/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          790.9203s
    total number of events:              10000
    total time taken by event execution: 790.3350
    per-request statistics:
         min:                                  1.54ms
         avg:                                 79.03ms
         max:                                713.63ms
         approx.  95 percentile:             245.63ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   790.3350/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (26363.63 ops/sec)

102400.00 MB transferred (25.75 MB/sec)


Test execution summary:
    total time:                          3977.3575s
    total number of events:              104857600
    total time taken by event execution: 3114.8886
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.03ms
         max:                               1169.43ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   3114.8886/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (9.1593Mb/sec)
  586.20 Requests/sec executed

Test execution summary:
    total time:                          223.5967s
    total number of events:              131072
    total time taken by event execution: 223.2282
    per-request statistics:
         min:                                  0.00ms
         avg:                                  1.70ms
         max:                               1787.97ms
         approx.  95 percentile:              11.77ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   223.2282/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (11.572Mb/sec)
  740.60 Requests/sec executed

Test execution summary:
    total time:                          176.9798s
    total number of events:              131072
    total time taken by event execution: 157.6413
    per-request statistics:
         min:                                  0.02ms
         avg:                                  1.20ms
         max:                                549.09ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   157.6413/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          891.6391s
    total number of events:              10000
    total time taken by event execution: 891.1337
    per-request statistics:
         min:                                  1.51ms
         avg:                                 89.11ms
         max:                                777.68ms
         approx.  95 percentile:             289.68ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   891.1337/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (24891.92 ops/sec)

102400.00 MB transferred (24.31 MB/sec)


Test execution summary:
    total time:                          4212.5147s
    total number of events:              104857600
    total time taken by event execution: 3324.2572
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.03ms
         max:                               1000.05ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   3324.2572/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (7.3576Mb/sec)
  470.89 Requests/sec executed

Test execution summary:
    total time:                          278.3518s
    total number of events:              131072
    total time taken by event execution: 278.1131
    per-request statistics:
         min:                                  0.00ms
         avg:                                  2.12ms
         max:                              12790.30ms
         approx.  95 percentile:              11.79ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   278.1131/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (11.1Mb/sec)
  710.42 Requests/sec executed

Test execution summary:
    total time:                          184.4998s
    total number of events:              131072
    total time taken by event execution: 159.4720
    per-request statistics:
         min:                                  0.02ms
         avg:                                  1.22ms
         max:                                744.08ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   159.4720/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          896.2882s
    total number of events:              10000
    total time taken by event execution: 895.8150
    per-request statistics:
         min:                                  1.51ms
         avg:                                 89.58ms
         max:                                957.64ms
         approx.  95 percentile:             301.62ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   895.8150/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (26659.96 ops/sec)

102400.00 MB transferred (26.04 MB/sec)


Test execution summary:
    total time:                          3933.1490s
    total number of events:              104857600
    total time taken by event execution: 3113.9732
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.03ms
         max:                                860.10ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   3113.9732/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential read test
Threads started!
Done.

Operations performed:  131072 Read, 0 Write, 0 Other = 131072 Total
Read 2Gb  Written 0b  Total transferred 2Gb  (10.104Mb/sec)
  646.68 Requests/sec executed

Test execution summary:
    total time:                          202.6838s
    total number of events:              131072
    total time taken by event execution: 202.4624
    per-request statistics:
         min:                                  0.00ms
         avg:                                  1.54ms
         max:                               1173.44ms
         approx.  95 percentile:              11.79ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   202.4624/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

128 files, 16384Kb each, 2048Mb total
Creating files for the test...
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Extra file open flags: 0
128 files, 16Mb each
2Gb total file size
Block size 16Kb
Periodic FSYNC enabled, calling fsync() each 100 requests.
Calling fsync() at the end of test, Enabled.
Using synchronous I/O mode
Doing sequential write (creation) test
Threads started!
Done.

Operations performed:  0 Read, 131072 Write, 128 Other = 131200 Total
Read 0b  Written 2Gb  Total transferred 2Gb  (10.951Mb/sec)
  700.86 Requests/sec executed

Test execution summary:
    total time:                          187.0158s
    total number of events:              131072
    total time taken by event execution: 159.4392
    per-request statistics:
         min:                                  0.02ms
         avg:                                  1.22ms
         max:                                680.14ms
         approx.  95 percentile:               0.03ms

Threads fairness:
    events (avg/stddev):           131072.0000/0.00
    execution time (avg/stddev):   159.4392/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Removing test files...
