$ fio fio-rand-read.fio

file1: (g=0): rw=randread, bs=(R) 4096B-4096B, (W) 4096B-4096B, (T) 4096B-4096B, ioengine=libaio, iodepth=16
fio-3.16
Starting 1 process
file1: Laying out IO file (1 file / 10240MiB)

Jobs: 1 (f=1): [r(1)][100.0%][r=352KiB/s][r=88 IOPS][eta 00m:00s]
file1: (groupid=0, jobs=1): err= 0: pid=116511: Wed Aug 19 14:31:11 2020
  read: IOPS=79, BW=317KiB/s (325kB/s)(279MiB/900009msec)
    slat (usec): min=245, max=522679, avg=12584.68, stdev=11157.35
    clat (usec): min=14, max=1591.2k, avg=188959.24, stdev=93285.66
     lat (msec): min=10, max=1686, avg=201.55, stdev=97.89
    clat percentiles (msec):
     |  1.00th=[  129],  5.00th=[  140], 10.00th=[  146], 20.00th=[  153],
     | 30.00th=[  159], 40.00th=[  165], 50.00th=[  169], 60.00th=[  176],
     | 70.00th=[  184], 80.00th=[  201], 90.00th=[  226], 95.00th=[  271],
     | 99.00th=[  642], 99.50th=[  885], 99.90th=[ 1217], 99.95th=[ 1351],
     | 99.99th=[ 1485]
   bw (  KiB/s): min=   15, max=  432, per=100.00%, avg=317.33, stdev=78.45, samples=1799
   iops        : min=    3, max=  108, avg=79.26, stdev=19.61, samples=1799
  lat (usec)   : 20=0.01%
  lat (msec)   : 20=0.01%, 50=0.01%, 100=0.01%, 250=93.99%, 500=4.25%
  lat (msec)   : 750=1.03%, 1000=0.38%, 2000=0.34%
  cpu          : usr=0.30%, sys=1.01%, ctx=72418, majf=0, minf=28
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=71397,0,0,0 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=16

Run status group 0 (all jobs):
   READ: bw=317KiB/s (325kB/s), 317KiB/s-317KiB/s (325kB/s-325kB/s), io=279MiB (292MB), run=900009-900009msec

Disk stats (read/write):
  sda: ios=71694/2411, merge=201/2029, ticks=902690/80780, in_queue=836760, util=33.42%
