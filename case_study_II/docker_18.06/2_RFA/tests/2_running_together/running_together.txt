# Victim Container
$ root@9b42b912717d:/# python victim_crawler.py 


# Attacker Container:
$ root@10bafd9b1f34:/# sysbench --test=cpu run && sysbench --test=memory run
sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          14.7261s
    total number of events:              10000
    total time taken by event execution: 14.7212
    per-request statistics:
         min:                                  1.08ms
         avg:                                  1.47ms
         max:                                  3.99ms
         approx.  95 percentile:               2.66ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   14.7212/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (974812.64 ops/sec)

102400.00 MB transferred (951.97 MB/sec)


Test execution summary:
    total time:                          107.5669s
    total number of events:              104857600
    total time taken by event execution: 85.6943
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  3.21ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   85.6943/0.00


# Host Stats:
darkskull-pc - IP 192.168.225.238/24 Pub 47.11.135.69				Uptime: 1:30:37
							user system	idle iowait	steal	LOAD	4-core
CPU0 [|||||||||||100.0%]	76.1%	23.3%	0.0%	0.0%	0.0%	1 min:	1.83
CPU1 [|	7.9%]				6.3%	1.7%	92.1%	0.0%	0.0%	5 min:	2.01
CPU2 [|	12.3%]				5.8%	2.9%	87.7%	0.0%	0.0%	15 min:	1.72
CPU3 [|	9.2%]				3.1%	2.4%	90.8%	0.3%	0.0%
MEM	[||||	41.4%]	
SWAP [	0.0%]	


# Docker stats
CONTAINER ID	NAME	CPU %	MEM USAGE / LIMIT	BLOCK I/O	PIDS
9b42b912717d	victim	25.43%	4.609MiB / 6.989GiB	6.98MB / 7.6GB	2
10bafd9b1f34	attacker	78.38%	17.13MiB / 6.989GiB	87.1MB / 19.4MB	3