## Text to write to files
with open('Google.htm') as f:
	html=f.read()

time_taken=""

from time import time

## Running experiment 100 times
for i in range(100):
	s=time()
	## Saving the files 10 times
	for j in range(10):
		with open(str(j)+'.html','w+') as f:
			f.write(html)
	e=time()
	time_taken+=str(e-s)+'\n'
## Writing to a file
with open("time_taken_with_sync",'w+') as f:
	f.write(time_taken)