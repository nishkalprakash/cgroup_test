## To log the docker stats one at a time (this takes too much time)
# while true; 
# do 
# docker stats attacker victim --no-stream \
# |tail -n1 \
# | ts '[%Y-%m-%d %H:%M:%S] ' \
# | tee --append $Dc2/logs/cs2_docker.log;	
# sleep 0.1;
# done

## To log the docker stats without the top header row
docker stats attacker \
--format "{{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}\t{{.BlockIO}}\t{{.PIDs}}"  \
| ts '[%Y-%m-%d %H:%M:%S] ' \
| tee --append /home/darkskull/DarKSkuLL/cgroup_test/case_study_II/logs/cs2_docker_attacker.log &
docker stats victim \
--format "{{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}\t{{.BlockIO}}\t{{.PIDs}}"  \
| ts '[%Y-%m-%d %H:%M:%S] ' \
| tee --append /home/darkskull/DarKSkuLL/cgroup_test/case_study_II/logs/cs2_docker_victim.log