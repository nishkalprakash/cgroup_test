## This is for logging all cores combined
# /usr/bin/sar -u 2 \
#| tee --append $Dc2/logs/cs2_host.log

## sar -P ALL is for logging all cores separately, the following 1 is for logging every sec
## The -o is to dump the otput in a binary file to avg easily [TODO]
/usr/bin/sar -P ALL 1 \
-o /home/darkskull/DarKSkuLL/cgroup_test/case_study_II/logs/cs2_host_$(date +"%FT%H%M").sar \
| tee --append /home/darkskull/DarKSkuLL/cgroup_test/case_study_II/logs/cs2_host.log
