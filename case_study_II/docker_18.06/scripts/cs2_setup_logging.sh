## To setup Logging for Case Study II

cd $Dc2/services

sudo cp cs2_docker_logger.service /etc/systemd/system/cs2_docker_logger.service
sudo chmod 644 /etc/systemd/system/cs2_docker_logger.service

sudo cp cs2_host_logger.service /etc/systemd/system/cs2_host_logger.service
sudo chmod 644 /etc/systemd/system/cs2_host_logger.service

sudo systemctl daemon-reload