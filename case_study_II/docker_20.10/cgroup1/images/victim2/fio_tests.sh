#!/bin/bash 
default="results"

mkdir -p $default

if [ -z "$1" ]
    then 
    results_file= "$default/fio_results.txt"
else
    results_file="$1"
    mkdir -p $(dirname "${results_file}")
fi

for i in {1..3}
do
    sleep 10;
    fio fio_tests/fio-rand-read.fio \
        fio_tests/fio-rand-write.fio \
        fio_tests/fio-seq-read.fio \
        fio_tests/fio-seq-write.fio  >> "$results_file" 2>&1
    echo "FIO Test - PASS $i Success"
done

unset ubase
unset results_file