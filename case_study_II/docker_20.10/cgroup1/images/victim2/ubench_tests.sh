#!/bin/bash
default="$(pwd)/results"

mkdir -p $default

if [ -z "$1" ]
    then 
    results_file= "$default/unixbench_results.txt"
else
    results_file="$(pwd)/$1"
    mkdir -p $(dirname "${results_file}")
fi

cd /home/byte-unixbench/UnixBench/

for i in {1..3}
do
    sleep 10;
    ./Run shell1 execl spawn fstime -c 1 >> "$results_file" 2>&1
    echo "UnixBench Test - PASS $i Success"
done

unset ubase
unset results_file