#!/bin/bash 
ubase="results/fio_results"

mkdir -p $ubase

if [ -z "$1" ]
    then 
    results_file= "$ubase/fio_results.txt"
else
    results_file="$ubase/$1"
fi

fio fio_tests/fio-rand-read.fio \
    fio_tests/fio-rand-write.fio \
    fio_tests/fio-seq-read.fio \
    fio_tests/fio-seq-write.fio  >> "$results_file" 2>&1

unset ubase
unset results_file