#!/bin/bash
pw=$(pwd) 
ubase="$pw/results/unixbench_results"

mkdir -p $ubase

if [ -z "$1" ]
    then 
    results_file= "$ubase/unixbench_results.txt"
else
    results_file="$ubase/$1"
fi

cd /home/byte-unixbench/UnixBench/

./Run shell1 execl spawn fstime -c 1 >> "$results_file" 2>&1

unset ubase
unset results_file