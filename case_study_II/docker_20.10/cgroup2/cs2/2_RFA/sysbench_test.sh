#!/bin/bash 
ubase="results/sysbench_results"

mkdir -p $ubase

if [ -z "$1" ]
    then 
    results_file= "$ubase/sysbench_results.txt"
else
    results_file="$ubase/$1"
fi

for i in {1..3}
do
    ( sysbench --test=cpu run ; \
    sysbench --test=memory run \
    ) >> "$results_file" 2>&1
done

python extract_sysbench.py $results_file