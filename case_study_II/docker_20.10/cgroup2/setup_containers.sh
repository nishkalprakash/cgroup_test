# docker build . \
#     --no-cache \
#     --tag victim:cs2 \
#     --cpuset-cpus "0-0"

## Building the base image
docker build . \
    --tag victim:cs2


## Running the images as different named containers

docker run -it -d \
    --name "victim" \
    --mount src="$(pwd)/cs2",dst="/home/cs2",type=bind \
    --cpuset-cpus "0-0" \
    victim:cs2

docker run -it -d \
    --name "attacker1" \
    --mount src="$(pwd)/cs2",dst="/home/cs2",type=bind \
    --cpuset-cpus "1-1" \
    victim:cs2

docker run -it -d \
    --name "attacker" \
    --mount src="$(pwd)/cs2",dst="/home/cs2",type=bind \
    --cpuset-cpus "0-0" \
    victim:cs2

## To stop all the containers
docker stop `docker ps -q`
