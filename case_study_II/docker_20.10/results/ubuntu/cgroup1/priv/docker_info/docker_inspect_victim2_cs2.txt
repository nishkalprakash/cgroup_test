[
    {
        "Id": "sha256:2a2d14606d8cbf5f9959b2247bc783e442a25a66609512c1bcb42114d7b2fb2e",
        "RepoTags": [
            "victim2:cs2"
        ],
        "RepoDigests": [],
        "Parent": "sha256:d3ff91215c28e16c9431357e5bf39036ed8f27da59322550cb84db7022caf885",
        "Comment": "",
        "Created": "2021-03-02T09:03:46.169992208Z",
        "Container": "",
        "ContainerConfig": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/sh",
                "-c",
                "#(nop) COPY dir:2b5448c1e27c0c1ac10fafb6a92303b70ba7c7cce3cb2112de30ea052207ddb9 in /home "
            ],
            "Image": "sha256:d3ff91215c28e16c9431357e5bf39036ed8f27da59322550cb84db7022caf885",
            "Volumes": null,
            "WorkingDir": "/home",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": null
        },
        "DockerVersion": "20.10.4",
        "Author": "",
        "Config": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/bash"
            ],
            "Image": "sha256:d3ff91215c28e16c9431357e5bf39036ed8f27da59322550cb84db7022caf885",
            "Volumes": null,
            "WorkingDir": "/home",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": null
        },
        "Architecture": "amd64",
        "Os": "linux",
        "Size": 507254836,
        "VirtualSize": 507254836,
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/2f09685753c2d6a164789376f15b6b0fead64f12f05dea9327a31528574db50d/diff:/var/lib/docker/overlay2/099fb129d2a6a7add414dc42cf2107240653b6ea5300ebb529afe2666b0c1bfc/diff:/var/lib/docker/overlay2/df5c0f9d2bd548d607b8cf93a2ec2ca442b4eea4b6e57ef6d8e1ae8ab6daea8b/diff:/var/lib/docker/overlay2/9395b669698d81626a9a218e7ac9bf5438b960f0602b163a9aa2b8eef28511d0/diff:/var/lib/docker/overlay2/89a936cd1adbed7f8b576781ca74af003df88ef9c2cd5e74c69b4f6bece25557/diff:/var/lib/docker/overlay2/33ce4553898e3ca85b95008d36b3223503963466a5a0a1b63bfa07e52fc46c12/diff:/var/lib/docker/overlay2/d5b634617ada5a03c19cb99531fb2d69e4bb3c727cf151489016ff9f2fff1206/diff:/var/lib/docker/overlay2/4bf5c683d5d3203ea4e55ceb6dad4c61590a815c87d779507a47127dbd54543b/diff",
                "MergedDir": "/var/lib/docker/overlay2/863ba12dab087bfffb6ccf715d82e1884346d8522e3800324b30766435801eb0/merged",
                "UpperDir": "/var/lib/docker/overlay2/863ba12dab087bfffb6ccf715d82e1884346d8522e3800324b30766435801eb0/diff",
                "WorkDir": "/var/lib/docker/overlay2/863ba12dab087bfffb6ccf715d82e1884346d8522e3800324b30766435801eb0/work"
            },
            "Name": "overlay2"
        },
        "RootFS": {
            "Type": "layers",
            "Layers": [
                "sha256:935c56d8b3f96d6587f3640e491767688b790c458a01fef327188abcbbafdc9a",
                "sha256:697949baa6589708187ff25708ee6c7cf2c6657399552a019598b1da7f617acf",
                "sha256:e6feec0db89a854a83e704823cdd269ad82625cb57ecc06b4ede6095948f66db",
                "sha256:5276d2b930fc59425e6cf44315e0ca0de5948865d615de79e34d5ff9bf3a9b96",
                "sha256:6a5d3297345271fc3d86c39f859379de157eaac883e395eb13db5ff6b711a4c2",
                "sha256:deb8019ad8f65461e65c415177a5e484aad12af1f9b9a532ff7d63b991a67fff",
                "sha256:a31fab31be0e841238b626587c8fcd543d3d7e81ba04e674f109e315009373b8",
                "sha256:d290545b0cbc215b9cd9016d10fbf42163a51fd30d91d0659297a829d0da44c0",
                "sha256:53a8339e46c0d8b6cf9eeac08e6dbdfd50c0de672161d3b447ee339befe2bfc6"
            ]
        },
        "Metadata": {
            "LastTagTime": "2021-03-02T14:33:47.129747906+05:30"
        }
    }
]
