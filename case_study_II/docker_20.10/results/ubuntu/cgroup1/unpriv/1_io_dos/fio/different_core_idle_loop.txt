file1: (g=0): rw=randread, bs=4K-4K/4K-4K/4K-4K, ioengine=libaio, iodepth=16
file1: (g=1): rw=randwrite, bs=4K-4K/4K-4K/4K-4K, ioengine=libaio, iodepth=16
...
file1: (g=2): rw=read, bs=256K-256K/256K-256K/256K-256K, ioengine=libaio, iodepth=16
file1: (g=3): rw=write, bs=256K-256K/256K-256K/256K-256K, ioengine=libaio, iodepth=16
fio-2.2.10
Starting 7 processes
file1: Laying out IO file(s) (1 file(s) / 1024MB)
file1: Laying out IO file(s) (1 file(s) / 1024MB)
file1: Laying out IO file(s) (1 file(s) / 1024MB)
file1: Laying out IO file(s) (1 file(s) / 1024MB)

file1: (groupid=0, jobs=1): err= 0: pid=17: Sun Feb 21 19:52:54 2021
  read : io=244312KB, bw=416952B/s, iops=101, runt=600009msec
    slat (usec): min=65, max=244826, avg=9815.45, stdev=4926.76
    clat (usec): min=10, max=486662, avg=147337.38, stdev=22456.84
     lat (msec): min=12, max=498, avg=157.15, stdev=23.22
    clat percentiles (msec):
     |  1.00th=[  102],  5.00th=[  114], 10.00th=[  121], 20.00th=[  129],
     | 30.00th=[  137], 40.00th=[  141], 50.00th=[  147], 60.00th=[  153],
     | 70.00th=[  157], 80.00th=[  165], 90.00th=[  176], 95.00th=[  184],
     | 99.00th=[  204], 99.50th=[  210], 99.90th=[  255], 99.95th=[  306],
     | 99.99th=[  465]
    bw (KB  /s): min=  250, max=  533, per=100.00%, avg=407.10, stdev=34.65
    lat (usec) : 20=0.01%
    lat (msec) : 20=0.01%, 50=0.01%, 100=0.75%, 250=99.12%, 500=0.12%
  cpu          : usr=0.10%, sys=0.37%, ctx=61255, majf=0, minf=24
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=61078/w=0/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=18: Sun Feb 21 19:52:54 2021
  write: io=20029MB, bw=34183KB/s, iops=8545, runt=600000msec
    slat (usec): min=3, max=141022, avg=12.50, stdev=257.56
    clat (usec): min=3, max=148379, avg=251.42, stdev=1078.72
     lat (usec): min=6, max=148384, avg=264.14, stdev=1111.00
    clat percentiles (usec):
     |  1.00th=[   93],  5.00th=[  115], 10.00th=[  139], 20.00th=[  145],
     | 30.00th=[  149], 40.00th=[  155], 50.00th=[  163], 60.00th=[  173],
     | 70.00th=[  187], 80.00th=[  201], 90.00th=[  227], 95.00th=[  245],
     | 99.00th=[  692], 99.50th=[ 8160], 99.90th=[12736], 99.95th=[16192],
     | 99.99th=[25472]
    bw (KB  /s): min=    0, max=328952, per=100.00%, avg=210088.04, stdev=82381.85
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=1.95%
    lat (usec) : 250=94.30%, 500=2.70%, 750=0.05%, 1000=0.02%
    lat (msec) : 2=0.05%, 4=0.08%, 10=0.56%, 20=0.24%, 50=0.03%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.71%, sys=7.89%, ctx=154770, majf=0, minf=9
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5127503/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=19: Sun Feb 21 19:52:54 2021
  write: io=20006MB, bw=34144KB/s, iops=8535, runt=600001msec
    slat (usec): min=2, max=187725, avg=12.57, stdev=275.24
    clat (usec): min=2, max=187884, avg=252.52, stdev=1135.61
     lat (usec): min=6, max=187890, avg=265.30, stdev=1169.57
    clat percentiles (usec):
     |  1.00th=[   95],  5.00th=[  122], 10.00th=[  139], 20.00th=[  145],
     | 30.00th=[  151], 40.00th=[  157], 50.00th=[  163], 60.00th=[  175],
     | 70.00th=[  187], 80.00th=[  203], 90.00th=[  227], 95.00th=[  243],
     | 99.00th=[  660], 99.50th=[ 8160], 99.90th=[12352], 99.95th=[16320],
     | 99.99th=[24192]
    bw (KB  /s): min=    0, max=344368, per=100.00%, avg=208758.94, stdev=81901.64
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=1.73%
    lat (usec) : 250=94.61%, 500=2.62%, 750=0.06%, 1000=0.02%
    lat (msec) : 2=0.05%, 4=0.08%, 10=0.56%, 20=0.24%, 50=0.03%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.75%, sys=7.79%, ctx=155751, majf=0, minf=11
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5121600/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=20: Sun Feb 21 19:52:54 2021
  write: io=19980MB, bw=34099KB/s, iops=8524, runt=600000msec
    slat (usec): min=3, max=189630, avg=12.37, stdev=264.32
    clat (usec): min=2, max=189776, avg=250.21, stdev=1104.09
     lat (usec): min=6, max=189783, avg=262.78, stdev=1136.25
    clat percentiles (usec):
     |  1.00th=[   96],  5.00th=[  125], 10.00th=[  141], 20.00th=[  145],
     | 30.00th=[  151], 40.00th=[  157], 50.00th=[  163], 60.00th=[  173],
     | 70.00th=[  187], 80.00th=[  203], 90.00th=[  227], 95.00th=[  245],
     | 99.00th=[  540], 99.50th=[ 8032], 99.90th=[13248], 99.95th=[16320],
     | 99.99th=[26496]
    bw (KB  /s): min=    0, max=340368, per=100.00%, avg=211942.71, stdev=85711.62
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=1.33%
    lat (usec) : 250=94.92%, 500=2.73%, 750=0.06%, 1000=0.02%
    lat (msec) : 2=0.05%, 4=0.08%, 10=0.53%, 20=0.24%, 50=0.03%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.73%, sys=7.88%, ctx=155076, majf=0, minf=10
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5114780/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=21: Sun Feb 21 19:52:54 2021
  write: io=20011MB, bw=34153KB/s, iops=8538, runt=600000msec
    slat (usec): min=3, max=108126, avg=12.54, stdev=259.35
    clat (usec): min=3, max=146578, avg=251.25, stdev=1101.16
     lat (usec): min=6, max=146584, avg=264.00, stdev=1134.11
    clat percentiles (usec):
     |  1.00th=[   96],  5.00th=[  126], 10.00th=[  139], 20.00th=[  145],
     | 30.00th=[  151], 40.00th=[  155], 50.00th=[  163], 60.00th=[  173],
     | 70.00th=[  187], 80.00th=[  203], 90.00th=[  227], 95.00th=[  245],
     | 99.00th=[  636], 99.50th=[ 8160], 99.90th=[12992], 99.95th=[16320],
     | 99.99th=[27264]
    bw (KB  /s): min=    0, max=345720, per=100.00%, avg=209314.17, stdev=84111.02
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=1.43%
    lat (usec) : 250=94.81%, 500=2.72%, 750=0.06%, 1000=0.02%
    lat (msec) : 2=0.05%, 4=0.09%, 10=0.54%, 20=0.24%, 50=0.03%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.71%, sys=7.92%, ctx=154331, majf=0, minf=11
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5122939/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=2, jobs=1): err= 0: pid=22: Sun Feb 21 19:52:54 2021
  read : io=46828MB, bw=79914KB/s, iops=312, runt=600046msec
    slat (usec): min=8, max=174, avg=25.85, stdev= 4.08
    clat (msec): min=15, max=364, avg=51.21, stdev=13.39
     lat (msec): min=15, max=364, avg=51.24, stdev=13.39
    clat percentiles (msec):
     |  1.00th=[   47],  5.00th=[   47], 10.00th=[   47], 20.00th=[   47],
     | 30.00th=[   49], 40.00th=[   49], 50.00th=[   49], 60.00th=[   49],
     | 70.00th=[   49], 80.00th=[   49], 90.00th=[   50], 95.00th=[   81],
     | 99.00th=[  104], 99.50th=[  131], 99.90th=[  192], 99.95th=[  237],
     | 99.99th=[  281]
    bw (KB  /s): min=16157, max=85504, per=100.00%, avg=80061.09, stdev=8333.88
    lat (msec) : 20=0.04%, 50=90.05%, 100=8.49%, 250=1.39%, 500=0.03%
  cpu          : usr=0.30%, sys=1.18%, ctx=187587, majf=0, minf=1033
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.2%, 16=99.6%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=187313/w=0/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=3, jobs=1): err= 0: pid=23: Sun Feb 21 19:52:54 2021
  write: io=44032MB, bw=73836KB/s, iops=288, runt=610661msec
    slat (usec): min=85, max=64618, avg=281.08, stdev=1207.80
    clat (usec): min=3, max=66799, avg=4267.99, stdev=6495.12
     lat (usec): min=94, max=66955, avg=4549.36, stdev=6926.20
    clat percentiles (usec):
     |  1.00th=[ 1416],  5.00th=[ 1448], 10.00th=[ 1480], 20.00th=[ 2224],
     | 30.00th=[ 2256], 40.00th=[ 2256], 50.00th=[ 2256], 60.00th=[ 2288],
     | 70.00th=[ 2288], 80.00th=[ 2384], 90.00th=[ 4512], 95.00th=[24192],
     | 99.00th=[35584], 99.50th=[35584], 99.90th=[35584], 99.95th=[38144],
     | 99.99th=[63744]
    bw (KB  /s): min=   18, max=1893376, per=100.00%, avg=680580.70, stdev=720945.17
    lat (usec) : 4=0.01%, 10=0.02%, 20=0.01%, 100=0.01%, 250=0.02%
    lat (usec) : 500=0.03%, 750=0.01%, 1000=0.01%
    lat (msec) : 2=10.89%, 4=79.01%, 10=0.03%, 20=3.47%, 50=6.48%
    lat (msec) : 100=0.02%
  cpu          : usr=0.32%, sys=6.01%, ctx=27007, majf=0, minf=10
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.2%, 16=99.6%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=176129/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16

Run status group 0 (all jobs):
   READ: io=244312KB, aggrb=407KB/s, minb=407KB/s, maxb=407KB/s, mint=600009msec, maxt=600009msec

Run status group 1 (all jobs):
  WRITE: io=80027MB, aggrb=136578KB/s, minb=34098KB/s, maxb=34183KB/s, mint=600000msec, maxt=600001msec

Run status group 2 (all jobs):
   READ: io=46828MB, aggrb=79914KB/s, minb=79914KB/s, maxb=79914KB/s, mint=600046msec, maxt=600046msec

Run status group 3 (all jobs):
  WRITE: io=44032MB, aggrb=73836KB/s, minb=73836KB/s, maxb=73836KB/s, mint=610661msec, maxt=610661msec
file1: (g=0): rw=randread, bs=4K-4K/4K-4K/4K-4K, ioengine=libaio, iodepth=16
file1: (g=1): rw=randwrite, bs=4K-4K/4K-4K/4K-4K, ioengine=libaio, iodepth=16
...
file1: (g=2): rw=read, bs=256K-256K/256K-256K/256K-256K, ioengine=libaio, iodepth=16
file1: (g=3): rw=write, bs=256K-256K/256K-256K/256K-256K, ioengine=libaio, iodepth=16
fio-2.2.10
Starting 7 processes

file1: (groupid=0, jobs=1): err= 0: pid=30: Sun Feb 21 20:33:19 2021
  read : io=244604KB, bw=417455B/s, iops=101, runt=600003msec
    slat (usec): min=65, max=257652, avg=9802.89, stdev=4907.03
    clat (usec): min=9, max=477548, avg=147149.31, stdev=22106.17
     lat (msec): min=5, max=488, avg=156.95, stdev=22.85
    clat percentiles (msec):
     |  1.00th=[  102],  5.00th=[  114], 10.00th=[  121], 20.00th=[  129],
     | 30.00th=[  137], 40.00th=[  141], 50.00th=[  147], 60.00th=[  151],
     | 70.00th=[  157], 80.00th=[  165], 90.00th=[  176], 95.00th=[  184],
     | 99.00th=[  202], 99.50th=[  210], 99.90th=[  239], 99.95th=[  262],
     | 99.99th=[  465]
    bw (KB  /s): min=  225, max=  505, per=100.00%, avg=407.65, stdev=33.37
    lat (usec) : 10=0.01%
    lat (msec) : 10=0.01%, 20=0.01%, 50=0.01%, 100=0.73%, 250=99.18%
    lat (msec) : 500=0.07%
  cpu          : usr=0.09%, sys=0.39%, ctx=61334, majf=0, minf=24
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=61151/w=0/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=31: Sun Feb 21 20:33:19 2021
  write: io=20480MB, bw=34104KB/s, iops=8526, runt=614925msec
    slat (usec): min=3, max=104326, avg=12.15, stdev=228.47
    clat (usec): min=3, max=105949, avg=246.11, stdev=966.99
     lat (usec): min=6, max=105956, avg=258.48, stdev=994.79
    clat percentiles (usec):
     |  1.00th=[   96],  5.00th=[  129], 10.00th=[  141], 20.00th=[  145],
     | 30.00th=[  151], 40.00th=[  155], 50.00th=[  163], 60.00th=[  173],
     | 70.00th=[  185], 80.00th=[  201], 90.00th=[  227], 95.00th=[  245],
     | 99.00th=[  414], 99.50th=[ 8160], 99.90th=[12992], 99.95th=[16192],
     | 99.99th=[24192]
    bw (KB  /s): min=    0, max=332904, per=100.00%, avg=212546.29, stdev=85591.27
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=1.65%
    lat (usec) : 250=94.70%, 500=2.70%, 750=0.04%, 1000=0.02%
    lat (msec) : 2=0.04%, 4=0.07%, 10=0.52%, 20=0.24%, 50=0.03%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.69%, sys=7.90%, ctx=155654, majf=0, minf=11
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5242881/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=32: Sun Feb 21 20:33:19 2021
  write: io=20480MB, bw=34104KB/s, iops=8526, runt=614924msec
    slat (usec): min=3, max=116313, avg=12.19, stdev=225.64
    clat (usec): min=3, max=116486, avg=246.63, stdev=952.86
     lat (usec): min=6, max=116493, avg=259.02, stdev=980.09
    clat percentiles (usec):
     |  1.00th=[   99],  5.00th=[  131], 10.00th=[  141], 20.00th=[  145],
     | 30.00th=[  151], 40.00th=[  155], 50.00th=[  163], 60.00th=[  173],
     | 70.00th=[  185], 80.00th=[  201], 90.00th=[  227], 95.00th=[  243],
     | 99.00th=[  442], 99.50th=[ 8160], 99.90th=[12608], 99.95th=[16192],
     | 99.99th=[24192]
    bw (KB  /s): min=    0, max=360262, per=100.00%, avg=212695.14, stdev=85590.12
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=1.03%
    lat (usec) : 250=95.34%, 500=2.65%, 750=0.04%, 1000=0.02%
    lat (msec) : 2=0.03%, 4=0.07%, 10=0.55%, 20=0.24%, 50=0.02%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.74%, sys=7.92%, ctx=158287, majf=0, minf=11
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5242881/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=33: Sun Feb 21 20:33:19 2021
  write: io=20480MB, bw=34104KB/s, iops=8526, runt=614925msec
    slat (usec): min=3, max=105350, avg=12.25, stdev=230.87
    clat (usec): min=2, max=106951, avg=248.10, stdev=985.03
     lat (usec): min=6, max=106959, avg=260.57, stdev=1012.99
    clat percentiles (usec):
     |  1.00th=[   90],  5.00th=[  117], 10.00th=[  139], 20.00th=[  145],
     | 30.00th=[  149], 40.00th=[  155], 50.00th=[  163], 60.00th=[  173],
     | 70.00th=[  185], 80.00th=[  201], 90.00th=[  227], 95.00th=[  243],
     | 99.00th=[  450], 99.50th=[ 8160], 99.90th=[13632], 99.95th=[16320],
     | 99.99th=[24192]
    bw (KB  /s): min=    0, max=356080, per=100.00%, avg=209837.03, stdev=83803.22
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=2.05%
    lat (usec) : 250=94.30%, 500=2.66%, 750=0.04%, 1000=0.02%
    lat (msec) : 2=0.04%, 4=0.06%, 10=0.54%, 20=0.25%, 50=0.03%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.69%, sys=7.86%, ctx=155852, majf=0, minf=11
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5242881/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=34: Sun Feb 21 20:33:19 2021
  write: io=20480MB, bw=34104KB/s, iops=8526, runt=614924msec
    slat (usec): min=3, max=104281, avg=12.23, stdev=229.58
    clat (usec): min=2, max=111132, avg=247.33, stdev=971.09
     lat (usec): min=6, max=111140, avg=259.78, stdev=998.98
    clat percentiles (usec):
     |  1.00th=[   94],  5.00th=[  124], 10.00th=[  139], 20.00th=[  145],
     | 30.00th=[  151], 40.00th=[  155], 50.00th=[  163], 60.00th=[  173],
     | 70.00th=[  185], 80.00th=[  201], 90.00th=[  225], 95.00th=[  243],
     | 99.00th=[  426], 99.50th=[ 8160], 99.90th=[12864], 99.95th=[16320],
     | 99.99th=[22400]
    bw (KB  /s): min=    0, max=351187, per=100.00%, avg=211587.39, stdev=84767.42
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=1.70%
    lat (usec) : 250=94.70%, 500=2.64%, 750=0.03%, 1000=0.01%
    lat (msec) : 2=0.03%, 4=0.07%, 10=0.55%, 20=0.24%, 50=0.03%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.66%, sys=7.95%, ctx=157772, majf=0, minf=11
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5242881/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=2, jobs=1): err= 0: pid=35: Sun Feb 21 20:33:19 2021
  read : io=49759MB, bw=84915KB/s, iops=331, runt=600046msec
    slat (usec): min=10, max=2305, avg=31.42, stdev= 6.60
    clat (msec): min=15, max=445, avg=48.19, stdev= 4.71
     lat (msec): min=15, max=445, avg=48.22, stdev= 4.71
    clat percentiles (msec):
     |  1.00th=[   47],  5.00th=[   47], 10.00th=[   47], 20.00th=[   47],
     | 30.00th=[   49], 40.00th=[   49], 50.00th=[   49], 60.00th=[   49],
     | 70.00th=[   49], 80.00th=[   49], 90.00th=[   49], 95.00th=[   49],
     | 99.00th=[   53], 99.50th=[   56], 99.90th=[   90], 99.95th=[  125],
     | 99.99th=[  221]
    bw (KB  /s): min=39623, max=87377, per=100.00%, avg=85020.35, stdev=2221.41
    lat (msec) : 20=0.05%, 50=97.81%, 100=2.08%, 250=0.05%, 500=0.01%
  cpu          : usr=0.36%, sys=1.46%, ctx=199772, majf=0, minf=1033
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.2%, 16=99.6%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=199035/w=0/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=3, jobs=1): err= 0: pid=36: Sun Feb 21 20:33:19 2021
  write: io=45056MB, bw=75865KB/s, iops=296, runt=608157msec
    slat (usec): min=87, max=14865, avg=267.13, stdev=1091.44
    clat (usec): min=3, max=39022, avg=4058.91, stdev=5959.00
     lat (usec): min=95, max=42804, avg=4326.33, stdev=6359.36
    clat percentiles (usec):
     |  1.00th=[ 2192],  5.00th=[ 2224], 10.00th=[ 2224], 20.00th=[ 2256],
     | 30.00th=[ 2256], 40.00th=[ 2256], 50.00th=[ 2288], 60.00th=[ 2288],
     | 70.00th=[ 2320], 80.00th=[ 2384], 90.00th=[ 3792], 95.00th=[22400],
     | 99.00th=[35072], 99.50th=[35584], 99.90th=[35584], 99.95th=[35584],
     | 99.99th=[39168]
    bw (KB  /s): min=   19, max=1700864, per=100.00%, avg=688960.89, stdev=686895.89
    lat (usec) : 4=0.01%, 10=0.02%, 100=0.01%, 250=0.02%, 500=0.03%
    lat (usec) : 750=0.01%, 1000=0.01%
    lat (msec) : 2=0.01%, 4=91.31%, 10=0.02%, 20=3.09%, 50=5.47%
  cpu          : usr=0.39%, sys=6.51%, ctx=235993, majf=0, minf=10
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.2%, 16=99.6%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=180225/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16

Run status group 0 (all jobs):
   READ: io=244604KB, aggrb=407KB/s, minb=407KB/s, maxb=407KB/s, mint=600003msec, maxt=600003msec

Run status group 1 (all jobs):
  WRITE: io=81920MB, aggrb=136416KB/s, minb=34104KB/s, maxb=34104KB/s, mint=614924msec, maxt=614925msec

Run status group 2 (all jobs):
   READ: io=49759MB, aggrb=84915KB/s, minb=84915KB/s, maxb=84915KB/s, mint=600046msec, maxt=600046msec

Run status group 3 (all jobs):
  WRITE: io=45056MB, aggrb=75864KB/s, minb=75864KB/s, maxb=75864KB/s, mint=608157msec, maxt=608157msec
file1: (g=0): rw=randread, bs=4K-4K/4K-4K/4K-4K, ioengine=libaio, iodepth=16
file1: (g=1): rw=randwrite, bs=4K-4K/4K-4K/4K-4K, ioengine=libaio, iodepth=16
...
file1: (g=2): rw=read, bs=256K-256K/256K-256K/256K-256K, ioengine=libaio, iodepth=16
file1: (g=3): rw=write, bs=256K-256K/256K-256K/256K-256K, ioengine=libaio, iodepth=16
fio-2.2.10
Starting 7 processes

file1: (groupid=0, jobs=1): err= 0: pid=43: Sun Feb 21 21:13:52 2021
  read : io=244368KB, bw=417049B/s, iops=101, runt=600007msec
    slat (usec): min=66, max=297633, avg=9812.02, stdev=4938.29
    clat (usec): min=10, max=490082, avg=147294.73, stdev=22196.02
     lat (msec): min=21, max=501, avg=157.11, stdev=22.95
    clat percentiles (msec):
     |  1.00th=[  102],  5.00th=[  114], 10.00th=[  121], 20.00th=[  130],
     | 30.00th=[  137], 40.00th=[  141], 50.00th=[  147], 60.00th=[  153],
     | 70.00th=[  157], 80.00th=[  165], 90.00th=[  176], 95.00th=[  184],
     | 99.00th=[  202], 99.50th=[  212], 99.90th=[  243], 99.95th=[  277],
     | 99.99th=[  482]
    bw (KB  /s): min=  199, max=  540, per=100.00%, avg=407.21, stdev=34.24
    lat (usec) : 20=0.01%
    lat (msec) : 50=0.01%, 100=0.73%, 250=99.18%, 500=0.08%
  cpu          : usr=0.11%, sys=0.39%, ctx=61305, majf=0, minf=24
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=61092/w=0/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=44: Sun Feb 21 21:13:52 2021
  write: io=20480MB, bw=33678KB/s, iops=8419, runt=622710msec
    slat (usec): min=3, max=121922, avg=12.22, stdev=239.71
    clat (usec): min=3, max=122079, avg=246.35, stdev=1000.30
     lat (usec): min=6, max=122086, avg=258.79, stdev=1029.68
    clat percentiles (usec):
     |  1.00th=[   98],  5.00th=[  126], 10.00th=[  139], 20.00th=[  145],
     | 30.00th=[  149], 40.00th=[  155], 50.00th=[  161], 60.00th=[  171],
     | 70.00th=[  185], 80.00th=[  201], 90.00th=[  225], 95.00th=[  243],
     | 99.00th=[  446], 99.50th=[ 8160], 99.90th=[12480], 99.95th=[16192],
     | 99.99th=[22912]
    bw (KB  /s): min=    0, max=332496, per=100.00%, avg=213428.04, stdev=84765.09
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=1.15%
    lat (usec) : 250=95.18%, 500=2.69%, 750=0.04%, 1000=0.03%
    lat (msec) : 2=0.04%, 4=0.07%, 10=0.52%, 20=0.25%, 50=0.02%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.58%, sys=7.83%, ctx=159409, majf=0, minf=10
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5242881/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=45: Sun Feb 21 21:13:52 2021
  write: io=20480MB, bw=33678KB/s, iops=8419, runt=622710msec
    slat (usec): min=3, max=123424, avg=12.26, stdev=244.51
    clat (usec): min=2, max=123576, avg=247.33, stdev=1021.29
     lat (usec): min=6, max=123582, avg=259.82, stdev=1051.18
    clat percentiles (usec):
     |  1.00th=[   95],  5.00th=[  119], 10.00th=[  139], 20.00th=[  145],
     | 30.00th=[  149], 40.00th=[  155], 50.00th=[  161], 60.00th=[  171],
     | 70.00th=[  185], 80.00th=[  199], 90.00th=[  225], 95.00th=[  243],
     | 99.00th=[  450], 99.50th=[ 8160], 99.90th=[13120], 99.95th=[16320],
     | 99.99th=[24192]
    bw (KB  /s): min=    0, max=335896, per=100.00%, avg=212223.53, stdev=84866.02
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=1.65%
    lat (usec) : 250=94.78%, 500=2.60%, 750=0.04%, 1000=0.02%
    lat (msec) : 2=0.04%, 4=0.08%, 10=0.53%, 20=0.24%, 50=0.03%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.62%, sys=7.80%, ctx=157930, majf=0, minf=12
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5242881/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=46: Sun Feb 21 21:13:52 2021
  write: io=20480MB, bw=33678KB/s, iops=8419, runt=622711msec
    slat (usec): min=3, max=136009, avg=12.26, stdev=242.50
    clat (usec): min=2, max=136170, avg=246.64, stdev=1012.38
     lat (usec): min=6, max=136177, avg=259.11, stdev=1041.98
    clat percentiles (usec):
     |  1.00th=[   90],  5.00th=[  118], 10.00th=[  139], 20.00th=[  145],
     | 30.00th=[  149], 40.00th=[  155], 50.00th=[  163], 60.00th=[  173],
     | 70.00th=[  185], 80.00th=[  201], 90.00th=[  225], 95.00th=[  243],
     | 99.00th=[  446], 99.50th=[ 8160], 99.90th=[12480], 99.95th=[16192],
     | 99.99th=[23168]
    bw (KB  /s): min=    0, max=351688, per=100.00%, avg=212760.27, stdev=84295.63
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=2.08%
    lat (usec) : 250=94.31%, 500=2.64%, 750=0.03%, 1000=0.02%
    lat (msec) : 2=0.04%, 4=0.07%, 10=0.55%, 20=0.23%, 50=0.03%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.67%, sys=7.70%, ctx=157564, majf=0, minf=12
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5242881/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=47: Sun Feb 21 21:13:52 2021
  write: io=20480MB, bw=33678KB/s, iops=8419, runt=622711msec
    slat (usec): min=3, max=121942, avg=12.22, stdev=244.19
    clat (usec): min=3, max=122093, avg=246.41, stdev=1018.15
     lat (usec): min=6, max=122100, avg=258.84, stdev=1048.06
    clat percentiles (usec):
     |  1.00th=[   95],  5.00th=[  122], 10.00th=[  139], 20.00th=[  145],
     | 30.00th=[  149], 40.00th=[  155], 50.00th=[  161], 60.00th=[  171],
     | 70.00th=[  183], 80.00th=[  199], 90.00th=[  225], 95.00th=[  243],
     | 99.00th=[  462], 99.50th=[ 8096], 99.90th=[12992], 99.95th=[16320],
     | 99.99th=[24192]
    bw (KB  /s): min=    0, max=360349, per=100.00%, avg=215072.99, stdev=88171.16
    lat (usec) : 4=0.01%, 10=0.01%, 20=0.01%, 50=0.01%, 100=1.80%
    lat (usec) : 250=94.65%, 500=2.56%, 750=0.04%, 1000=0.02%
    lat (msec) : 2=0.04%, 4=0.08%, 10=0.52%, 20=0.25%, 50=0.03%
    lat (msec) : 100=0.01%, 250=0.01%
  cpu          : usr=2.72%, sys=7.64%, ctx=157774, majf=0, minf=11
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=100.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=5242881/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=2, jobs=1): err= 0: pid=48: Sun Feb 21 21:13:52 2021
  read : io=49745MB, bw=84892KB/s, iops=331, runt=600048msec
    slat (usec): min=11, max=120, avg=31.90, stdev= 3.59
    clat (msec): min=15, max=350, avg=48.20, stdev= 4.98
     lat (msec): min=15, max=350, avg=48.23, stdev= 4.98
    clat percentiles (msec):
     |  1.00th=[   47],  5.00th=[   47], 10.00th=[   47], 20.00th=[   47],
     | 30.00th=[   49], 40.00th=[   49], 50.00th=[   49], 60.00th=[   49],
     | 70.00th=[   49], 80.00th=[   49], 90.00th=[   49], 95.00th=[   49],
     | 99.00th=[   53], 99.50th=[   57], 99.90th=[   88], 99.95th=[  104],
     | 99.99th=[  338]
    bw (KB  /s): min=25396, max=86016, per=100.00%, avg=84994.84, stdev=2737.99
    lat (msec) : 20=0.05%, 50=97.79%, 100=2.11%, 250=0.04%, 500=0.02%
  cpu          : usr=0.33%, sys=1.51%, ctx=199599, majf=0, minf=1033
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.2%, 16=99.6%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=198981/w=0/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=3, jobs=1): err= 0: pid=49: Sun Feb 21 21:13:52 2021
  write: io=45056MB, bw=75852KB/s, iops=296, runt=608262msec
    slat (usec): min=86, max=300044, avg=266.72, stdev=1283.54
    clat (usec): min=4, max=335295, avg=4052.55, stdev=6527.74
     lat (usec): min=97, max=335445, avg=4319.57, stdev=6926.03
    clat percentiles (usec):
     |  1.00th=[ 2224],  5.00th=[ 2224], 10.00th=[ 2224], 20.00th=[ 2256],
     | 30.00th=[ 2256], 40.00th=[ 2256], 50.00th=[ 2288], 60.00th=[ 2288],
     | 70.00th=[ 2320], 80.00th=[ 2384], 90.00th=[ 3760], 95.00th=[22400],
     | 99.00th=[35072], 99.50th=[35072], 99.90th=[35584], 99.95th=[35584],
     | 99.99th=[39168]
    bw (KB  /s): min=   18, max=1705984, per=100.00%, avg=690225.37, stdev=700483.60
    lat (usec) : 10=0.02%, 20=0.01%, 100=0.01%, 250=0.02%, 500=0.02%
    lat (usec) : 750=0.01%, 1000=0.01%
    lat (msec) : 2=0.01%, 4=91.48%, 10=0.04%, 20=3.13%, 50=5.25%
    lat (msec) : 500=0.01%
  cpu          : usr=0.38%, sys=6.51%, ctx=233747, majf=0, minf=11
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.2%, 16=99.6%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=180225/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16

Run status group 0 (all jobs):
   READ: io=244368KB, aggrb=407KB/s, minb=407KB/s, maxb=407KB/s, mint=600007msec, maxt=600007msec

Run status group 1 (all jobs):
  WRITE: io=81920MB, aggrb=134711KB/s, minb=33677KB/s, maxb=33677KB/s, mint=622710msec, maxt=622711msec

Run status group 2 (all jobs):
   READ: io=49745MB, aggrb=84891KB/s, minb=84891KB/s, maxb=84891KB/s, mint=600048msec, maxt=600048msec

Run status group 3 (all jobs):
  WRITE: io=45056MB, aggrb=75851KB/s, minb=75851KB/s, maxb=75851KB/s, mint=608262msec, maxt=608262msec
