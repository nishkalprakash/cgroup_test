gcc -o pgms/arithoh -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Darithoh src/arith.c 
gcc -o pgms/register -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum='register int' src/arith.c 
gcc -o pgms/short -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=short src/arith.c 
gcc -o pgms/int -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=int src/arith.c 
gcc -o pgms/long -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=long src/arith.c 
gcc -o pgms/float -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=float src/arith.c 
gcc -o pgms/double -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=double src/arith.c 
gcc -o pgms/hanoi -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/hanoi.c 
gcc -o pgms/syscall -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/syscall.c 
gcc -o pgms/context1 -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/context1.c 
gcc -o pgms/pipe -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/pipe.c 
src/pipe.c: In function 'main':
src/pipe.c:52:2: warning: ignoring return value of 'pipe', declared with attribute warn_unused_result [-Wunused-result]
  pipe(pvec);
  ^
gcc -o pgms/spawn -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/spawn.c 
gcc -o pgms/execl -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/execl.c 
In file included from src/execl.c:34:0:
src/big.c: In function 'dummy':
src/big.c:109:5: warning: ignoring return value of 'freopen', declared with attribute warn_unused_result [-Wunused-result]
     freopen("masterlog.00", "a", stderr);
     ^
src/big.c:197:6: warning: ignoring return value of 'freopen', declared with attribute warn_unused_result [-Wunused-result]
      freopen(logname, "w", stderr);
      ^
src/big.c:221:3: warning: ignoring return value of 'dup', declared with attribute warn_unused_result [-Wunused-result]
   dup(pvec[0]);
   ^
src/big.c:225:6: warning: ignoring return value of 'freopen', declared with attribute warn_unused_result [-Wunused-result]
      freopen(logname, "w", stderr);
      ^
src/big.c:318:4: warning: ignoring return value of 'write', declared with attribute warn_unused_result [-Wunused-result]
    write(fcopy, cp->line, p - cp->line + 1);
    ^
gcc -o pgms/dhry2 -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -DHZ= ./src/dhry_1.c ./src/dhry_2.c
gcc -o pgms/dhry2reg -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -DHZ= -DREG=register ./src/dhry_1.c ./src/dhry_2.c
gcc -o pgms/looper -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/looper.c 
gcc -o pgms/fstime -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/fstime.c 
gcc -o pgms/whetstone-double -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -DDP -DGTODay -DUNIXBENCH src/whets.c -lm
make all
make[1]: Entering directory '/home/byte-unixbench/UnixBench'
make distr
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
Checking distribution of files
./pgms  exists
./src  exists
./testdir  exists
./tmp  exists
./results  exists
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make programs
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
make[2]: Nothing to be done for 'programs'.
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make[1]: Leaving directory '/home/byte-unixbench/UnixBench'
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
sh: 1: 3dinfo: not found

   #    #  #    #  #  #    #          #####   ######  #    #   ####   #    #
   #    #  ##   #  #   #  #           #    #  #       ##   #  #    #  #    #
   #    #  # #  #  #    ##            #####   #####   # #  #  #       ######
   #    #  #  # #  #    ##            #    #  #       #  # #  #       #    #
   #    #  #   ##  #   #  #           #    #  #       #   ##  #    #  #    #
    ####   #    #  #  #    #          #####   ######  #    #   ####   #    #

   Version 5.1.3                      Based on the Byte Magazine Unix Benchmark

   Multi-CPU version                  Version 5 revisions by Ian Smith,
                                      Sunnyvale, CA, USA
   January 13, 2011                   johantheghost at yahoo period com

------------------------------------------------------------------------------
   Use directories for:
      * File I/O tests (named fs***) = /home/byte-unixbench/UnixBench/tmp
      * Results                      = /home/byte-unixbench/UnixBench/results
------------------------------------------------------------------------------


1 x Shell Scripts (1 concurrent)  1 2 3

1 x Execl Throughput  1 2 3

1 x Process Creation  1 2 3

1 x File Copy 1024 bufsize 2000 maxblocks  1 2 3

========================================================================
   BYTE UNIX Benchmarks (Version 5.1.3)

   System: 67f0e4510a39: GNU/Linux
   OS: GNU/Linux -- 5.8.0-44-generic -- #50~20.04.1-Ubuntu SMP Wed Feb 10 21:07:30 UTC 2021
   Machine: x86_64 (x86_64)
   Language: en_US.utf8 (charmap="ANSI_X3.4-1968", collate="ANSI_X3.4-1968")
   CPU 0: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   CPU 1: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   CPU 2: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   CPU 3: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   14:32:58 up 22:10,  0 users,  load average: 2.63, 2.58, 2.73; runlevel 

------------------------------------------------------------------------
Benchmark Run: Mon Mar 01 2021 14:32:58 - 14:41:52
4 CPUs in system; running 1 parallel copy of tests

Execl Throughput                               2194.5 lps   (29.9 s, 2 samples)
File Copy 1024 bufsize 2000 maxblocks        188547.0 KBps  (30.0 s, 2 samples)
Process Creation                               4846.1 lps   (30.0 s, 2 samples)
Shell Scripts (1 concurrent)                   3926.5 lpm   (60.0 s, 2 samples)

System Benchmarks Partial Index              BASELINE       RESULT    INDEX
Execl Throughput                                 43.0       2194.5    510.3
File Copy 1024 bufsize 2000 maxblocks          3960.0     188547.0    476.1
Process Creation                                126.0       4846.1    384.6
Shell Scripts (1 concurrent)                     42.4       3926.5    926.1
                                                                   ========
System Benchmarks Index Score (Partial Only)                          542.4

make all
make[1]: Entering directory '/home/byte-unixbench/UnixBench'
make distr
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
Checking distribution of files
./pgms  exists
./src  exists
./testdir  exists
./tmp  exists
./results  exists
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make programs
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
make[2]: Nothing to be done for 'programs'.
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make[1]: Leaving directory '/home/byte-unixbench/UnixBench'
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
sh: 1: 3dinfo: not found

   #    #  #    #  #  #    #          #####   ######  #    #   ####   #    #
   #    #  ##   #  #   #  #           #    #  #       ##   #  #    #  #    #
   #    #  # #  #  #    ##            #####   #####   # #  #  #       ######
   #    #  #  # #  #    ##            #    #  #       #  # #  #       #    #
   #    #  #   ##  #   #  #           #    #  #       #   ##  #    #  #    #
    ####   #    #  #  #    #          #####   ######  #    #   ####   #    #

   Version 5.1.3                      Based on the Byte Magazine Unix Benchmark

   Multi-CPU version                  Version 5 revisions by Ian Smith,
                                      Sunnyvale, CA, USA
   January 13, 2011                   johantheghost at yahoo period com

------------------------------------------------------------------------------
   Use directories for:
      * File I/O tests (named fs***) = /home/byte-unixbench/UnixBench/tmp
      * Results                      = /home/byte-unixbench/UnixBench/results
------------------------------------------------------------------------------


1 x Shell Scripts (1 concurrent)  1 2 3

1 x Execl Throughput  1 2 3

1 x Process Creation  1 2 3

1 x File Copy 1024 bufsize 2000 maxblocks  1 2 3

========================================================================
   BYTE UNIX Benchmarks (Version 5.1.3)

   System: 67f0e4510a39: GNU/Linux
   OS: GNU/Linux -- 5.8.0-44-generic -- #50~20.04.1-Ubuntu SMP Wed Feb 10 21:07:30 UTC 2021
   Machine: x86_64 (x86_64)
   Language: en_US.utf8 (charmap="ANSI_X3.4-1968", collate="ANSI_X3.4-1968")
   CPU 0: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   CPU 1: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   CPU 2: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   CPU 3: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   14:41:52 up 22:19,  0 users,  load average: 1.80, 2.07, 2.41; runlevel 

------------------------------------------------------------------------
Benchmark Run: Mon Mar 01 2021 14:41:52 - 14:50:44
4 CPUs in system; running 1 parallel copy of tests

Execl Throughput                               2199.7 lps   (29.9 s, 2 samples)
File Copy 1024 bufsize 2000 maxblocks        188791.4 KBps  (30.0 s, 2 samples)
Process Creation                               4826.1 lps   (30.0 s, 2 samples)
Shell Scripts (1 concurrent)                   3927.2 lpm   (60.0 s, 2 samples)

System Benchmarks Partial Index              BASELINE       RESULT    INDEX
Execl Throughput                                 43.0       2199.7    511.6
File Copy 1024 bufsize 2000 maxblocks          3960.0     188791.4    476.7
Process Creation                                126.0       4826.1    383.0
Shell Scripts (1 concurrent)                     42.4       3927.2    926.2
                                                                   ========
System Benchmarks Index Score (Partial Only)                          542.4

make all
make[1]: Entering directory '/home/byte-unixbench/UnixBench'
make distr
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
Checking distribution of files
./pgms  exists
./src  exists
./testdir  exists
./tmp  exists
./results  exists
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make programs
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
make[2]: Nothing to be done for 'programs'.
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make[1]: Leaving directory '/home/byte-unixbench/UnixBench'
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
sh: 1: 3dinfo: not found

   #    #  #    #  #  #    #          #####   ######  #    #   ####   #    #
   #    #  ##   #  #   #  #           #    #  #       ##   #  #    #  #    #
   #    #  # #  #  #    ##            #####   #####   # #  #  #       ######
   #    #  #  # #  #    ##            #    #  #       #  # #  #       #    #
   #    #  #   ##  #   #  #           #    #  #       #   ##  #    #  #    #
    ####   #    #  #  #    #          #####   ######  #    #   ####   #    #

   Version 5.1.3                      Based on the Byte Magazine Unix Benchmark

   Multi-CPU version                  Version 5 revisions by Ian Smith,
                                      Sunnyvale, CA, USA
   January 13, 2011                   johantheghost at yahoo period com

------------------------------------------------------------------------------
   Use directories for:
      * File I/O tests (named fs***) = /home/byte-unixbench/UnixBench/tmp
      * Results                      = /home/byte-unixbench/UnixBench/results
------------------------------------------------------------------------------


1 x Shell Scripts (1 concurrent)  1 2 3

1 x Execl Throughput  1 2 3

1 x Process Creation  1 2 3

1 x File Copy 1024 bufsize 2000 maxblocks  1 2 3

========================================================================
   BYTE UNIX Benchmarks (Version 5.1.3)

   System: 67f0e4510a39: GNU/Linux
   OS: GNU/Linux -- 5.8.0-44-generic -- #50~20.04.1-Ubuntu SMP Wed Feb 10 21:07:30 UTC 2021
   Machine: x86_64 (x86_64)
   Language: en_US.utf8 (charmap="ANSI_X3.4-1968", collate="ANSI_X3.4-1968")
   CPU 0: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   CPU 1: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   CPU 2: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   CPU 3: Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz (4789.2 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET, Intel virtualization
   14:50:44 up 22:28,  0 users,  load average: 1.79, 1.96, 2.23; runlevel 

------------------------------------------------------------------------
Benchmark Run: Mon Mar 01 2021 14:50:44 - 14:59:36
4 CPUs in system; running 1 parallel copy of tests

Execl Throughput                               2198.3 lps   (29.9 s, 2 samples)
File Copy 1024 bufsize 2000 maxblocks        188295.3 KBps  (30.0 s, 2 samples)
Process Creation                               4853.1 lps   (30.0 s, 2 samples)
Shell Scripts (1 concurrent)                   3929.3 lpm   (60.0 s, 2 samples)

System Benchmarks Partial Index              BASELINE       RESULT    INDEX
Execl Throughput                                 43.0       2198.3    511.2
File Copy 1024 bufsize 2000 maxblocks          3960.0     188295.3    475.5
Process Creation                                126.0       4853.1    385.2
Shell Scripts (1 concurrent)                     42.4       3929.3    926.7
                                                                   ========
System Benchmarks Index Score (Partial Only)                          542.7

