sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.5915s
    total number of events:              10000
    total time taken by event execution: 13.5902
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.36ms
         max:                                  3.03ms
         approx.  95 percentile:               1.36ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.5902/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2169610.81 ops/sec)

102400.00 MB transferred (2118.76 MB/sec)


Test execution summary:
    total time:                          48.3301s
    total number of events:              104857600
    total time taken by event execution: 38.0956
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.09ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.0956/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.5874s
    total number of events:              10000
    total time taken by event execution: 13.5862
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.36ms
         max:                                  1.64ms
         approx.  95 percentile:               1.36ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.5862/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2169511.11 ops/sec)

102400.00 MB transferred (2118.66 MB/sec)


Test execution summary:
    total time:                          48.3324s
    total number of events:              104857600
    total time taken by event execution: 38.0942
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.08ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.0942/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.5990s
    total number of events:              10000
    total time taken by event execution: 13.5978
    per-request statistics:
         min:                                  1.36ms
         avg:                                  1.36ms
         max:                                  1.65ms
         approx.  95 percentile:               1.37ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.5978/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2169128.51 ops/sec)

102400.00 MB transferred (2118.29 MB/sec)


Test execution summary:
    total time:                          48.3409s
    total number of events:              104857600
    total time taken by event execution: 38.1009
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.11ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.1009/0.00

