gcc -o pgms/arithoh -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Darithoh src/arith.c 
gcc -o pgms/register -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum='register int' src/arith.c 
gcc -o pgms/short -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=short src/arith.c 
gcc -o pgms/int -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=int src/arith.c 
gcc -o pgms/long -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=long src/arith.c 
gcc -o pgms/float -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=float src/arith.c 
gcc -o pgms/double -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=double src/arith.c 
gcc -o pgms/hanoi -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/hanoi.c 
gcc -o pgms/syscall -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/syscall.c 
gcc -o pgms/context1 -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/context1.c 
gcc -o pgms/pipe -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/pipe.c 
src/pipe.c: In function 'main':
src/pipe.c:52:2: warning: ignoring return value of 'pipe', declared with attribute warn_unused_result [-Wunused-result]
  pipe(pvec);
  ^
gcc -o pgms/spawn -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/spawn.c 
gcc -o pgms/execl -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/execl.c 
In file included from src/execl.c:34:0:
src/big.c: In function 'dummy':
src/big.c:109:5: warning: ignoring return value of 'freopen', declared with attribute warn_unused_result [-Wunused-result]
     freopen("masterlog.00", "a", stderr);
     ^
src/big.c:197:6: warning: ignoring return value of 'freopen', declared with attribute warn_unused_result [-Wunused-result]
      freopen(logname, "w", stderr);
      ^
src/big.c:221:3: warning: ignoring return value of 'dup', declared with attribute warn_unused_result [-Wunused-result]
   dup(pvec[0]);
   ^
src/big.c:225:6: warning: ignoring return value of 'freopen', declared with attribute warn_unused_result [-Wunused-result]
      freopen(logname, "w", stderr);
      ^
src/big.c:318:4: warning: ignoring return value of 'write', declared with attribute warn_unused_result [-Wunused-result]
    write(fcopy, cp->line, p - cp->line + 1);
    ^
gcc -o pgms/dhry2 -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -DHZ= ./src/dhry_1.c ./src/dhry_2.c
gcc -o pgms/dhry2reg -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -DHZ= -DREG=register ./src/dhry_1.c ./src/dhry_2.c
gcc -o pgms/looper -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/looper.c 
gcc -o pgms/fstime -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/fstime.c 
gcc -o pgms/whetstone-double -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -DDP -DGTODay -DUNIXBENCH src/whets.c -lm
make all
make[1]: Entering directory '/home/byte-unixbench/UnixBench'
make distr
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
Checking distribution of files
./pgms  exists
./src  exists
./testdir  exists
./tmp  exists
./results  exists
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make programs
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
make[2]: Nothing to be done for 'programs'.
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make[1]: Leaving directory '/home/byte-unixbench/UnixBench'
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
sh: 1: 3dinfo: not found

   #    #  #    #  #  #    #          #####   ######  #    #   ####   #    #
   #    #  ##   #  #   #  #           #    #  #       ##   #  #    #  #    #
   #    #  # #  #  #    ##            #####   #####   # #  #  #       ######
   #    #  #  # #  #    ##            #    #  #       #  # #  #       #    #
   #    #  #   ##  #   #  #           #    #  #       #   ##  #    #  #    #
    ####   #    #  #  #    #          #####   ######  #    #   ####   #    #

   Version 5.1.3                      Based on the Byte Magazine Unix Benchmark

   Multi-CPU version                  Version 5 revisions by Ian Smith,
                                      Sunnyvale, CA, USA
   January 13, 2011                   johantheghost at yahoo period com

------------------------------------------------------------------------------
   Use directories for:
      * File I/O tests (named fs***) = /home/byte-unixbench/UnixBench/tmp
      * Results                      = /home/byte-unixbench/UnixBench/results
------------------------------------------------------------------------------


1 x Shell Scripts (1 concurrent)  1 2 3

1 x Execl Throughput  1 2 3

1 x Process Creation  1 2 3

1 x File Copy 1024 bufsize 2000 maxblocks  1 2 3

========================================================================
   BYTE UNIX Benchmarks (Version 5.1.3)

   System: 43ef8173501a: GNU/Linux
   OS: GNU/Linux -- 5.4.72-microsoft-standard-WSL2 -- #1 SMP Wed Oct 28 23:40:43 UTC 2020
   Machine: x86_64 (x86_64)
   Language: en_US.utf8 (charmap="ANSI_X3.4-1968", collate="ANSI_X3.4-1968")
   CPU 0: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 1: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 2: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 3: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   21:16:33 up  8:12,  0 users,  load average: 0.82, 2.12, 2.70; runlevel 

------------------------------------------------------------------------
Benchmark Run: Wed Feb 10 2021 21:16:34 - 21:25:35
4 CPUs in system; running 1 parallel copy of tests

Execl Throughput                               2653.0 lps   (29.8 s, 2 samples)
File Copy 1024 bufsize 2000 maxblocks        201717.0 KBps  (30.0 s, 2 samples)
Process Creation                               5551.0 lps   (30.0 s, 2 samples)
Shell Scripts (1 concurrent)                   4611.7 lpm   (60.0 s, 2 samples)

System Benchmarks Partial Index              BASELINE       RESULT    INDEX
Execl Throughput                                 43.0       2653.0    617.0
File Copy 1024 bufsize 2000 maxblocks          3960.0     201717.0    509.4
Process Creation                                126.0       5551.0    440.6
Shell Scripts (1 concurrent)                     42.4       4611.7   1087.7
                                                                   ========
System Benchmarks Index Score (Partial Only)                          623.0

make all
make[1]: Entering directory '/home/byte-unixbench/UnixBench'
make distr
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
Checking distribution of files
./pgms  exists
./src  exists
./testdir  exists
./tmp  exists
./results  exists
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make programs
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
make[2]: Nothing to be done for 'programs'.
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make[1]: Leaving directory '/home/byte-unixbench/UnixBench'
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
sh: 1: 3dinfo: not found

   #    #  #    #  #  #    #          #####   ######  #    #   ####   #    #
   #    #  ##   #  #   #  #           #    #  #       ##   #  #    #  #    #
   #    #  # #  #  #    ##            #####   #####   # #  #  #       ######
   #    #  #  # #  #    ##            #    #  #       #  # #  #       #    #
   #    #  #   ##  #   #  #           #    #  #       #   ##  #    #  #    #
    ####   #    #  #  #    #          #####   ######  #    #   ####   #    #

   Version 5.1.3                      Based on the Byte Magazine Unix Benchmark

   Multi-CPU version                  Version 5 revisions by Ian Smith,
                                      Sunnyvale, CA, USA
   January 13, 2011                   johantheghost at yahoo period com

------------------------------------------------------------------------------
   Use directories for:
      * File I/O tests (named fs***) = /home/byte-unixbench/UnixBench/tmp
      * Results                      = /home/byte-unixbench/UnixBench/results
------------------------------------------------------------------------------


1 x Shell Scripts (1 concurrent)  1 2 3

1 x Execl Throughput  1 2 3

1 x Process Creation  1 2 3

1 x File Copy 1024 bufsize 2000 maxblocks  1 2 3

========================================================================
   BYTE UNIX Benchmarks (Version 5.1.3)

   System: 43ef8173501a: GNU/Linux
   OS: GNU/Linux -- 5.4.72-microsoft-standard-WSL2 -- #1 SMP Wed Oct 28 23:40:43 UTC 2020
   Machine: x86_64 (x86_64)
   Language: en_US.utf8 (charmap="ANSI_X3.4-1968", collate="ANSI_X3.4-1968")
   CPU 0: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 1: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 2: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 3: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   21:25:39 up  8:21,  0 users,  load average: 1.83, 2.11, 2.53; runlevel 

------------------------------------------------------------------------
Benchmark Run: Wed Feb 10 2021 21:25:39 - 21:34:45
4 CPUs in system; running 1 parallel copy of tests

Execl Throughput                               2620.2 lps   (29.6 s, 2 samples)
File Copy 1024 bufsize 2000 maxblocks        202727.6 KBps  (30.0 s, 2 samples)
Process Creation                               5447.9 lps   (30.0 s, 2 samples)
Shell Scripts (1 concurrent)                   5188.3 lpm   (60.0 s, 2 samples)

System Benchmarks Partial Index              BASELINE       RESULT    INDEX
Execl Throughput                                 43.0       2620.2    609.4
File Copy 1024 bufsize 2000 maxblocks          3960.0     202727.6    511.9
Process Creation                                126.0       5447.9    432.4
Shell Scripts (1 concurrent)                     42.4       5188.3   1223.6
                                                                   ========
System Benchmarks Index Score (Partial Only)                          637.4

make all
make[1]: Entering directory '/home/byte-unixbench/UnixBench'
make distr
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
Checking distribution of files
./pgms  exists
./src  exists
./testdir  exists
./tmp  exists
./results  exists
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make programs
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
make[2]: Nothing to be done for 'programs'.
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make[1]: Leaving directory '/home/byte-unixbench/UnixBench'
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
sh: 1: 3dinfo: not found

   #    #  #    #  #  #    #          #####   ######  #    #   ####   #    #
   #    #  ##   #  #   #  #           #    #  #       ##   #  #    #  #    #
   #    #  # #  #  #    ##            #####   #####   # #  #  #       ######
   #    #  #  # #  #    ##            #    #  #       #  # #  #       #    #
   #    #  #   ##  #   #  #           #    #  #       #   ##  #    #  #    #
    ####   #    #  #  #    #          #####   ######  #    #   ####   #    #

   Version 5.1.3                      Based on the Byte Magazine Unix Benchmark

   Multi-CPU version                  Version 5 revisions by Ian Smith,
                                      Sunnyvale, CA, USA
   January 13, 2011                   johantheghost at yahoo period com

------------------------------------------------------------------------------
   Use directories for:
      * File I/O tests (named fs***) = /home/byte-unixbench/UnixBench/tmp
      * Results                      = /home/byte-unixbench/UnixBench/results
------------------------------------------------------------------------------


1 x Shell Scripts (1 concurrent)  1 2 3

1 x Execl Throughput  1 2 3

1 x Process Creation  1 2 3

1 x File Copy 1024 bufsize 2000 maxblocks  1 2 3

========================================================================
   BYTE UNIX Benchmarks (Version 5.1.3)

   System: 43ef8173501a: GNU/Linux
   OS: GNU/Linux -- 5.4.72-microsoft-standard-WSL2 -- #1 SMP Wed Oct 28 23:40:43 UTC 2020
   Machine: x86_64 (x86_64)
   Language: en_US.utf8 (charmap="ANSI_X3.4-1968", collate="ANSI_X3.4-1968")
   CPU 0: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 1: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 2: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 3: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   21:34:46 up  8:30,  0 users,  load average: 1.77, 1.99, 2.31; runlevel 

------------------------------------------------------------------------
Benchmark Run: Wed Feb 10 2021 21:34:46 - 21:43:52
4 CPUs in system; running 1 parallel copy of tests

Execl Throughput                               2722.1 lps   (29.5 s, 2 samples)
File Copy 1024 bufsize 2000 maxblocks        202052.0 KBps  (30.0 s, 2 samples)
Process Creation                               5452.2 lps   (30.0 s, 2 samples)
Shell Scripts (1 concurrent)                   5076.8 lpm   (60.0 s, 2 samples)

System Benchmarks Partial Index              BASELINE       RESULT    INDEX
Execl Throughput                                 43.0       2722.1    633.0
File Copy 1024 bufsize 2000 maxblocks          3960.0     202052.0    510.2
Process Creation                                126.0       5452.2    432.7
Shell Scripts (1 concurrent)                     42.4       5076.8   1197.4
                                                                   ========
System Benchmarks Index Score (Partial Only)                          639.6

