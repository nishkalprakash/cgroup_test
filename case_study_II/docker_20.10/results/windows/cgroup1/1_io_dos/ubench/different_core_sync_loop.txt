gcc -o pgms/arithoh -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Darithoh src/arith.c 
gcc -o pgms/register -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum='register int' src/arith.c 
gcc -o pgms/short -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=short src/arith.c 
gcc -o pgms/int -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=int src/arith.c 
gcc -o pgms/long -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=long src/arith.c 
gcc -o pgms/float -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=float src/arith.c 
gcc -o pgms/double -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -Ddatum=double src/arith.c 
gcc -o pgms/hanoi -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/hanoi.c 
gcc -o pgms/syscall -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/syscall.c 
gcc -o pgms/context1 -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/context1.c 
gcc -o pgms/pipe -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/pipe.c 
src/pipe.c: In function 'main':
src/pipe.c:52:2: warning: ignoring return value of 'pipe', declared with attribute warn_unused_result [-Wunused-result]
  pipe(pvec);
  ^
gcc -o pgms/spawn -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/spawn.c 
gcc -o pgms/execl -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/execl.c 
In file included from src/execl.c:34:0:
src/big.c: In function 'dummy':
src/big.c:109:5: warning: ignoring return value of 'freopen', declared with attribute warn_unused_result [-Wunused-result]
     freopen("masterlog.00", "a", stderr);
     ^
src/big.c:197:6: warning: ignoring return value of 'freopen', declared with attribute warn_unused_result [-Wunused-result]
      freopen(logname, "w", stderr);
      ^
src/big.c:221:3: warning: ignoring return value of 'dup', declared with attribute warn_unused_result [-Wunused-result]
   dup(pvec[0]);
   ^
src/big.c:225:6: warning: ignoring return value of 'freopen', declared with attribute warn_unused_result [-Wunused-result]
      freopen(logname, "w", stderr);
      ^
src/big.c:318:4: warning: ignoring return value of 'write', declared with attribute warn_unused_result [-Wunused-result]
    write(fcopy, cp->line, p - cp->line + 1);
    ^
gcc -o pgms/dhry2 -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -DHZ= ./src/dhry_1.c ./src/dhry_2.c
gcc -o pgms/dhry2reg -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -DHZ= -DREG=register ./src/dhry_1.c ./src/dhry_2.c
gcc -o pgms/looper -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/looper.c 
gcc -o pgms/fstime -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME src/fstime.c 
gcc -o pgms/whetstone-double -Wall -pedantic -O3 -ffast-math -march=native -mtune=native -I ./src -DTIME -DDP -DGTODay -DUNIXBENCH src/whets.c -lm
make all
make[1]: Entering directory '/home/byte-unixbench/UnixBench'
make distr
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
Checking distribution of files
./pgms  exists
./src  exists
./testdir  exists
./tmp  exists
./results  exists
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make programs
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
make[2]: Nothing to be done for 'programs'.
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make[1]: Leaving directory '/home/byte-unixbench/UnixBench'
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
sh: 1: 3dinfo: not found

   #    #  #    #  #  #    #          #####   ######  #    #   ####   #    #
   #    #  ##   #  #   #  #           #    #  #       ##   #  #    #  #    #
   #    #  # #  #  #    ##            #####   #####   # #  #  #       ######
   #    #  #  # #  #    ##            #    #  #       #  # #  #       #    #
   #    #  #   ##  #   #  #           #    #  #       #   ##  #    #  #    #
    ####   #    #  #  #    #          #####   ######  #    #   ####   #    #

   Version 5.1.3                      Based on the Byte Magazine Unix Benchmark

   Multi-CPU version                  Version 5 revisions by Ian Smith,
                                      Sunnyvale, CA, USA
   January 13, 2011                   johantheghost at yahoo period com

------------------------------------------------------------------------------
   Use directories for:
      * File I/O tests (named fs***) = /home/byte-unixbench/UnixBench/tmp
      * Results                      = /home/byte-unixbench/UnixBench/results
------------------------------------------------------------------------------


1 x Shell Scripts (1 concurrent)  1 2 3

1 x Execl Throughput  1 2 3

1 x Process Creation  1 2 3

1 x File Copy 1024 bufsize 2000 maxblocks  1 2 3

========================================================================
   BYTE UNIX Benchmarks (Version 5.1.3)

   System: c37127c6578b: GNU/Linux
   OS: GNU/Linux -- 5.4.72-microsoft-standard-WSL2 -- #1 SMP Wed Oct 28 23:40:43 UTC 2020
   Machine: x86_64 (x86_64)
   Language: en_US.utf8 (charmap="ANSI_X3.4-1968", collate="ANSI_X3.4-1968")
   CPU 0: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 1: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 2: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 3: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   21:44:35 up  8:40,  0 users,  load average: 1.83, 1.93, 2.15; runlevel 

------------------------------------------------------------------------
Benchmark Run: Wed Feb 10 2021 21:44:35 - 21:53:28
4 CPUs in system; running 1 parallel copy of tests

Execl Throughput                               2759.7 lps   (29.9 s, 2 samples)
File Copy 1024 bufsize 2000 maxblocks        214000.8 KBps  (30.0 s, 2 samples)
Process Creation                               5613.0 lps   (30.0 s, 2 samples)
Shell Scripts (1 concurrent)                   5586.5 lpm   (60.0 s, 2 samples)

System Benchmarks Partial Index              BASELINE       RESULT    INDEX
Execl Throughput                                 43.0       2759.7    641.8
File Copy 1024 bufsize 2000 maxblocks          3960.0     214000.8    540.4
Process Creation                                126.0       5613.0    445.5
Shell Scripts (1 concurrent)                     42.4       5586.5   1317.6
                                                                   ========
System Benchmarks Index Score (Partial Only)                          671.7

make all
make[1]: Entering directory '/home/byte-unixbench/UnixBench'
make distr
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
Checking distribution of files
./pgms  exists
./src  exists
./testdir  exists
./tmp  exists
./results  exists
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make programs
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
make[2]: Nothing to be done for 'programs'.
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make[1]: Leaving directory '/home/byte-unixbench/UnixBench'
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
sh: 1: 3dinfo: not found

   #    #  #    #  #  #    #          #####   ######  #    #   ####   #    #
   #    #  ##   #  #   #  #           #    #  #       ##   #  #    #  #    #
   #    #  # #  #  #    ##            #####   #####   # #  #  #       ######
   #    #  #  # #  #    ##            #    #  #       #  # #  #       #    #
   #    #  #   ##  #   #  #           #    #  #       #   ##  #    #  #    #
    ####   #    #  #  #    #          #####   ######  #    #   ####   #    #

   Version 5.1.3                      Based on the Byte Magazine Unix Benchmark

   Multi-CPU version                  Version 5 revisions by Ian Smith,
                                      Sunnyvale, CA, USA
   January 13, 2011                   johantheghost at yahoo period com

------------------------------------------------------------------------------
   Use directories for:
      * File I/O tests (named fs***) = /home/byte-unixbench/UnixBench/tmp
      * Results                      = /home/byte-unixbench/UnixBench/results
------------------------------------------------------------------------------


1 x Shell Scripts (1 concurrent)  1 2 3

1 x Execl Throughput  1 2 3

1 x Process Creation  1 2 3

1 x File Copy 1024 bufsize 2000 maxblocks  1 2 3

========================================================================
   BYTE UNIX Benchmarks (Version 5.1.3)

   System: c37127c6578b: GNU/Linux
   OS: GNU/Linux -- 5.4.72-microsoft-standard-WSL2 -- #1 SMP Wed Oct 28 23:40:43 UTC 2020
   Machine: x86_64 (x86_64)
   Language: en_US.utf8 (charmap="ANSI_X3.4-1968", collate="ANSI_X3.4-1968")
   CPU 0: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 1: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 2: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 3: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   21:53:29 up  8:49,  0 users,  load average: 2.70, 2.46, 2.37; runlevel 

------------------------------------------------------------------------
Benchmark Run: Wed Feb 10 2021 21:53:29 - 22:02:24
4 CPUs in system; running 1 parallel copy of tests

Execl Throughput                               2738.4 lps   (29.9 s, 2 samples)
File Copy 1024 bufsize 2000 maxblocks        206799.3 KBps  (30.0 s, 2 samples)
Process Creation                               5513.3 lps   (30.0 s, 2 samples)
Shell Scripts (1 concurrent)                   5705.1 lpm   (60.0 s, 2 samples)

System Benchmarks Partial Index              BASELINE       RESULT    INDEX
Execl Throughput                                 43.0       2738.4    636.8
File Copy 1024 bufsize 2000 maxblocks          3960.0     206799.3    522.2
Process Creation                                126.0       5513.3    437.6
Shell Scripts (1 concurrent)                     42.4       5705.1   1345.5
                                                                   ========
System Benchmarks Index Score (Partial Only)                          665.2

make all
make[1]: Entering directory '/home/byte-unixbench/UnixBench'
make distr
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
Checking distribution of files
./pgms  exists
./src  exists
./testdir  exists
./tmp  exists
./results  exists
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make programs
make[2]: Entering directory '/home/byte-unixbench/UnixBench'
make[2]: Nothing to be done for 'programs'.
make[2]: Leaving directory '/home/byte-unixbench/UnixBench'
make[1]: Leaving directory '/home/byte-unixbench/UnixBench'
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
sh: 1: 3dinfo: not found

   #    #  #    #  #  #    #          #####   ######  #    #   ####   #    #
   #    #  ##   #  #   #  #           #    #  #       ##   #  #    #  #    #
   #    #  # #  #  #    ##            #####   #####   # #  #  #       ######
   #    #  #  # #  #    ##            #    #  #       #  # #  #       #    #
   #    #  #   ##  #   #  #           #    #  #       #   ##  #    #  #    #
    ####   #    #  #  #    #          #####   ######  #    #   ####   #    #

   Version 5.1.3                      Based on the Byte Magazine Unix Benchmark

   Multi-CPU version                  Version 5 revisions by Ian Smith,
                                      Sunnyvale, CA, USA
   January 13, 2011                   johantheghost at yahoo period com

------------------------------------------------------------------------------
   Use directories for:
      * File I/O tests (named fs***) = /home/byte-unixbench/UnixBench/tmp
      * Results                      = /home/byte-unixbench/UnixBench/results
------------------------------------------------------------------------------


1 x Shell Scripts (1 concurrent)  1 2 3

1 x Execl Throughput  1 2 3

1 x Process Creation  1 2 3

1 x File Copy 1024 bufsize 2000 maxblocks  1 2 3

========================================================================
   BYTE UNIX Benchmarks (Version 5.1.3)

   System: c37127c6578b: GNU/Linux
   OS: GNU/Linux -- 5.4.72-microsoft-standard-WSL2 -- #1 SMP Wed Oct 28 23:40:43 UTC 2020
   Machine: x86_64 (x86_64)
   Language: en_US.utf8 (charmap="ANSI_X3.4-1968", collate="ANSI_X3.4-1968")
   CPU 0: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 1: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 2: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   CPU 3: Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz (6185.9 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   22:02:24 up  8:58,  0 users,  load average: 2.65, 2.54, 2.47; runlevel 

------------------------------------------------------------------------
Benchmark Run: Wed Feb 10 2021 22:02:24 - 22:11:18
4 CPUs in system; running 1 parallel copy of tests

Execl Throughput                               2744.6 lps   (29.5 s, 2 samples)
File Copy 1024 bufsize 2000 maxblocks        208020.9 KBps  (30.0 s, 2 samples)
Process Creation                               5611.9 lps   (30.0 s, 2 samples)
Shell Scripts (1 concurrent)                   5749.3 lpm   (60.0 s, 2 samples)

System Benchmarks Partial Index              BASELINE       RESULT    INDEX
Execl Throughput                                 43.0       2744.6    638.3
File Copy 1024 bufsize 2000 maxblocks          3960.0     208020.9    525.3
Process Creation                                126.0       5611.9    445.4
Shell Scripts (1 concurrent)                     42.4       5749.3   1356.0
                                                                   ========
System Benchmarks Index Score (Partial Only)                          670.8

