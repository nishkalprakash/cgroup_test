#!/bin/bash

# echo "for linux"
# res="results"
# echo "for windows"
# res="results/win"

# START For 1_io_dos
echo "START For 1_io_dos"
base="results/win/1_io_dos"
mkdir -p $base
echo "Attacker Idle Loop on different core"
for loop in {'idle_loop','sync_loop'}
do
    att=$(docker run -d \
        --cpuset-cpus "1-1" \
        --cpu-period 200000 \
        --cpu-quota 200000 \
        attacker:cs2 bash $loop.sh)

    # for i in {'fio','ubench'}
    for i in 'ubench'
    do
        out="$base/$i"
        mkdir -p $out
        out="$out/different_core_$loop.txt"
        docker run -it \
            --cpuset-cpus "0-0" \
            --cpu-period 200000 \
            --cpu-quota 200000 \
            --mount src="$(pwd)/results",dst="/home/results",type=bind \
            victim2:cs2 bash "$i"_tests.sh "$out"

        python extract_$i.py "$out"
    done

    docker stop $att
done

echo "Removing all the containers "

docker stop $(docker ps -q)
docker rm $(docker ps -aq)

echo "END For 1_io_dos"
# END For 1_io_dos

################################echo ""


# START For 2_RFA
echo "START For 2_RFA"

base="results/win/2_RFA"
mkdir -p $base

echo "Attacker running sysbench test solo"
out="$base/no_competition_2.txt"
docker run -it \
        --cpuset-cpus "0-0" \
        --cpu-period 200000 \
        --cpu-quota 200000 \
        --mount src="$(pwd)/results",dst="/home/results",type=bind \
        attacker2:cs2 bash sysbench_test.sh $out
    
python extract_sysbench.py "$out"

echo "Victim running web_crawler"
vic=$(docker run -d \
    --cpuset-cpus "0-0" \
    --cpu-period 200000 \
    --cpu-quota 200000 \
    victim:cs2 python RFA_crawler.py)

# echo "Attacker running sysbench together with victim on same core"
echo "Attacker running sysbench together with victim on same core"
out="$base/running_together_2.txt"
# cmd="bash sysbench_test.sh $out"
docker run -it \
    --cpuset-cpus "0-0" \
    --cpu-period 200000 \
    --cpu-quota 200000 \
    --mount src="$(pwd)/results",dst="/home/results",type=bind \
    attacker2:cs2 bash sysbench_test.sh $out

python extract_sysbench.py "$out"

# echo "Attacker running sync loop and sysbench together with victim on same core"
echo "Attacker running sync loop and sysbench together with victim on same core"
out="$base/RFA_2.txt"
# cmd="bash sync_loop.sh &; bash sysbench_test.sh $out"
att=$(docker run -d \
    --cpuset-cpus "0-0" \
    --cpu-period 200000 \
    --cpu-quota 200000 \
    --mount src="$(pwd)/results",dst="/home/results",type=bind \
    attacker2:cs2 bash sync_loop.sh)
docker exec -it \
    $att bash sysbench_test.sh $out
    
    
python extract_sysbench.py "$out"


echo "Removing all the containers "
docker stop $(docker ps -q)
docker rm $(docker ps -aq)


echo "END of 2_RFA"
# END of 2_RFA

################################echo ""

# START covert channel 
echo "START covert channel"
base="results/win/3_covert"
mkdir -p $base
for i in {'','covert'};
do 
    if [ -z $i ] 
    then 
        out="$base/without_sync.txt"
    else
        out="$base/with_sync.txt"
        # cmd="bash sync_loop.sh && $cmd"
        att=$(docker run -d \
            --cpuset-cpus "0-0" \
            --cpu-period 200000 \
            --cpu-quota 200000 \
            --mount src="$(pwd)/results",dst="/home/results",type=bind \
            attacker:cs2 bash sync_loop.sh)
    fi
    docker run -it \
        --cpuset-cpus "0-0" \
        --cpu-period 200000 \
        --cpu-quota 200000 \
        --mount src="$(pwd)/results",dst="/home/results",type=bind \
        victim:cs2 python covert_test.py $out
    # python extract_sysbench.py "$out"
done

docker stop $(docker ps -q)


echo "END for 3_covert"
# END for 3_covert

echo "Removing all the containers "
docker stop $(docker ps -q)
docker rm $(docker ps -aq)