#!/bin/bash 
default="results"

mkdir -p $default

if [ -z "$1" ]
    then 
    results_file= "$default/sysbench_results.txt"
else
    results_file="$1"
    mkdir -p "$(dirname "${results_file}")"
fi

for i in {1..3}
do
    ( sysbench --test=cpu run ; \
    sysbench --test=memory run \
    ) >> "$results_file" 2>&1
    echo "Sysbench Test - PASS $i Success"

done

# python extract_sysbench.py $results_file