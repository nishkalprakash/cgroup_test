import sys
import os,shutil

## Text to write to files
with open('Google.htm') as f:
	html=f.read()

time_taken=""

from time import time
base="test_folder"
os.mkdir(base)
## Running experiment 100 times

for i in range(100):
	s=time()
	## Saving the files 10 times
	for j in range(10):
		with open(base+"/"+str(j)+'.html','w+') as f:
			f.write(html)
	e=time()
	time_taken+=str(e-s)+'\n'
shutil.rmtree(base)
## Writing to a file
try:
	out=sys.argv[1]
except IndexError:
	out=input("Enter the output file name")
with open(out,'w+') as f:
	f.write(time_taken)