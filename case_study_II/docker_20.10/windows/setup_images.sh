di="docker_info"
mkdir -p $di
docker info > $di/docker_info.txt 2>&1
## Building the base image
for i in $(ls images)
do
    docker build "images/$i" \
        --tag $i:cs2
    echo "Built image $i:cs2"
    docker inspect $i:cs2 > $di/"docker_inspect_$i"_cs2.txt 2>&1
done

# ## Running the images as different named containers

# docker run -it -d \
#     --name "victim" \
#     --mount src="$(pwd)/cs2",dst="/home/cs2",type=bind \
#     --cpuset-cpus "0-0" \
#     victim:cs2

# docker run -it -d \
#     --name "attacker1" \
#     --mount src="$(pwd)/cs2",dst="/home/cs2",type=bind \
#     --cpuset-cpus "1-1" \
#     victim:cs2

# docker run -it -d \
#     --name "attacker" \
#     --mount src="$(pwd)/cs2",dst="/home/cs2",type=bind \
#     --cpuset-cpus "0-0" \
#     victim:cs2

# ## To stop all the containers
# docker stop `docker ps -q`
