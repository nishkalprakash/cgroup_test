file1: (g=0): rw=randwrite, bs=4K-4K/4K-4K/4K-4K, ioengine=libaio, iodepth=16
...
file1: (g=1): rw=randread, bs=4K-4K/4K-4K/4K-4K, ioengine=libaio, iodepth=16
file1: (g=2): rw=read, bs=256K-256K/256K-256K/256K-256K, ioengine=libaio, iodepth=16
file1: (g=3): rw=write, bs=256K-256K/256K-256K/256K-256K, ioengine=libaio, iodepth=16
fio-2.2.10
Starting 7 processes

file1: (groupid=0, jobs=1): err= 0: pid=12699: Wed Nov 11 02:15:12 2020
  write: io=28520KB, bw=32393B/s, iops=7, runt=901550msec
    slat (usec): min=9, max=78600K, avg=126395.07, stdev=1803438.12
    clat (usec): min=15, max=78604K, avg=1889611.39, stdev=6877111.55
     lat (usec): min=308, max=78604K, avg=2016008.15, stdev=7089595.97
    clat percentiles (usec):
     |  1.00th=[  314],  5.00th=[  398], 10.00th=[  426], 20.00th=[  446],
     | 30.00th=[  462], 40.00th=[  490], 50.00th=[111104], 60.00th=[203776],
     | 70.00th=[296960], 80.00th=[610304], 90.00th=[3751936], 95.00th=[9895936],
     | 99.00th=[16711680], 99.50th=[16711680], 99.90th=[16711680], 99.95th=[16711680],
     | 99.99th=[16711680]
    bw (KB  /s): min=    0, max= 3005, per=100.00%, avg=136.34, stdev=261.14
    lat (usec) : 20=0.01%, 500=42.20%, 750=5.25%, 1000=0.21%
    lat (msec) : 2=0.06%, 4=0.17%, 10=0.43%, 20=0.49%, 50=0.77%
    lat (msec) : 100=0.29%, 250=19.68%, 500=8.50%, 750=2.83%, 1000=2.26%
    lat (msec) : 2000=3.16%, >=2000=13.69%
  cpu          : usr=0.00%, sys=0.03%, ctx=1127, majf=3, minf=22
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=99.8%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=7130/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=0, jobs=1): err= 0: pid=12700: Wed Nov 11 02:15:14 2020
  write: io=27956KB, bw=31752B/s, iops=7, runt=901574msec
    slat (usec): min=10, max=66466K, avg=128945.85, stdev=1802848.29
    clat (usec): min=23, max=67161K, avg=1919379.48, stdev=7136889.54
     lat (usec): min=309, max=67470K, avg=2048326.83, stdev=7360728.95
    clat percentiles (usec):
     |  1.00th=[  318],  5.00th=[  354], 10.00th=[  362], 20.00th=[  426],
     | 30.00th=[  438], 40.00th=[  462], 50.00th=[11328], 60.00th=[203776],
     | 70.00th=[407552], 80.00th=[610304], 90.00th=[2506752], 95.00th=[9109504],
     | 99.00th=[16711680], 99.50th=[16711680], 99.90th=[16711680], 99.95th=[16711680],
     | 99.99th=[16711680]
    bw (KB  /s): min=    0, max= 2077, per=100.00%, avg=150.29, stdev=227.71
    lat (usec) : 50=0.01%, 500=43.60%, 750=4.89%, 1000=0.21%
    lat (msec) : 2=0.31%, 4=0.44%, 10=0.11%, 20=1.50%, 50=1.10%
    lat (msec) : 100=0.26%, 250=15.60%, 500=9.39%, 750=6.10%, 1000=2.28%
    lat (msec) : 2000=3.08%, >=2000=11.12%
  cpu          : usr=0.01%, sys=0.03%, ctx=1246, majf=2, minf=21
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=99.8%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=6989/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=0, jobs=1): err= 0: pid=12701: Wed Nov 11 02:15:14 2020
  write: io=27884KB, bw=31671B/s, iops=7, runt=901552msec
    slat (usec): min=11, max=77296K, avg=129279.04, stdev=1872331.79
    clat (usec): min=15, max=77732K, avg=1917093.53, stdev=7187914.74
     lat (usec): min=337, max=77732K, avg=2046374.29, stdev=7412568.49
    clat percentiles (usec):
     |  1.00th=[  334],  5.00th=[  390], 10.00th=[  426], 20.00th=[  438],
     | 30.00th=[  458], 40.00th=[  482], 50.00th=[64768], 60.00th=[203776],
     | 70.00th=[257024], 80.00th=[667648], 90.00th=[3588096], 95.00th=[11075584],
     | 99.00th=[16711680], 99.50th=[16711680], 99.90th=[16711680], 99.95th=[16711680],
     | 99.99th=[16711680]
    bw (KB  /s): min=    0, max= 2342, per=100.00%, avg=152.31, stdev=260.83
    lat (usec) : 20=0.01%, 500=42.88%, 750=4.26%, 1000=0.49%
    lat (msec) : 2=0.44%, 4=0.27%, 10=0.03%, 20=0.07%, 50=1.18%
    lat (msec) : 100=0.65%, 250=19.32%, 500=6.73%, 750=3.93%, 1000=2.52%
    lat (msec) : 2000=4.10%, >=2000=13.11%
  cpu          : usr=0.01%, sys=0.03%, ctx=1278, majf=1, minf=21
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=99.8%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=6971/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=0, jobs=1): err= 0: pid=12702: Wed Nov 11 02:15:14 2020
  write: io=25808KB, bw=29311B/s, iops=7, runt=901615msec
    slat (usec): min=9, max=64966K, avg=139677.56, stdev=1929788.45
    clat (usec): min=16, max=65039K, avg=2062833.06, stdev=7612481.64
     lat (usec): min=306, max=65039K, avg=2202512.27, stdev=7851312.03
    clat percentiles (usec):
     |  1.00th=[  326],  5.00th=[  358], 10.00th=[  410], 20.00th=[  442],
     | 30.00th=[  458], 40.00th=[  490], 50.00th=[72192], 60.00th=[203776],
     | 70.00th=[203776], 80.00th=[407552], 90.00th=[3817472], 95.00th=[11862016],
     | 99.00th=[16711680], 99.50th=[16711680], 99.90th=[16711680], 99.95th=[16711680],
     | 99.99th=[16711680]
    bw (KB  /s): min=    0, max= 5114, per=100.00%, avg=198.55, stdev=460.34
    lat (usec) : 20=0.02%, 500=41.38%, 750=5.41%, 1000=0.02%
    lat (msec) : 2=0.33%, 4=0.25%, 10=0.05%, 20=0.15%, 50=1.70%
    lat (msec) : 100=1.19%, 250=23.73%, 500=8.42%, 750=2.93%, 1000=1.02%
    lat (msec) : 2000=1.26%, >=2000=12.15%
  cpu          : usr=0.00%, sys=0.03%, ctx=1097, majf=2, minf=21
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=99.8%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=6452/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=1, jobs=1): err= 0: pid=12703: Wed Nov 11 02:15:14 2020
  read : io=20500KB, bw=23313B/s, iops=5, runt=900407msec
    slat (usec): min=296, max=8897.3K, avg=175549.48, stdev=267602.86
    clat (usec): min=18, max=19194K, avg=2607869.25, stdev=1420144.83
     lat (msec): min=170, max=19209, avg=2783.42, stdev=1485.80
    clat percentiles (msec):
     |  1.00th=[  223],  5.00th=[  383], 10.00th=[  947], 20.00th=[ 1795],
     | 30.00th=[ 2376], 40.00th=[ 2573], 50.00th=[ 2704], 60.00th=[ 2868],
     | 70.00th=[ 2999], 80.00th=[ 3195], 90.00th=[ 3458], 95.00th=[ 3916],
     | 99.00th=[ 6652], 99.50th=[10421], 99.90th=[16712], 99.95th=[16712],
     | 99.99th=[16712]
    bw (KB  /s): min=    0, max=  229, per=100.00%, avg=23.76, stdev=19.01
    lat (usec) : 20=0.02%
    lat (msec) : 250=1.87%, 500=4.88%, 750=1.37%, 1000=2.50%, 2000=12.04%
    lat (msec) : >=2000=77.33%
  cpu          : usr=0.02%, sys=0.07%, ctx=5361, majf=10, minf=17
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.2%, 16=99.7%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=5125/w=0/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=2, jobs=1): err= 0: pid=12704: Wed Nov 11 02:15:14 2020
  read : io=1338.0MB, bw=1521.8KB/s, iops=5, runt=900377msec
    slat (usec): min=866, max=5170.6K, avg=167404.38, stdev=233461.54
    clat (usec): min=18, max=20637K, avg=2498511.64, stdev=1623064.30
     lat (msec): min=137, max=21904, avg=2665.92, stdev=1710.10
    clat percentiles (msec):
     |  1.00th=[  330],  5.00th=[ 1205], 10.00th=[ 1614], 20.00th=[ 1844],
     | 30.00th=[ 1975], 40.00th=[ 2089], 50.00th=[ 2212], 60.00th=[ 2343],
     | 70.00th=[ 2507], 80.00th=[ 2704], 90.00th=[ 3326], 95.00th=[ 4555],
     | 99.00th=[10421], 99.50th=[13304], 99.90th=[16712], 99.95th=[16712],
     | 99.99th=[16712]
    bw (KB  /s): min=   17, max=11821, per=100.00%, avg=1707.16, stdev=909.70
    lat (usec) : 20=0.02%, 50=0.02%
    lat (msec) : 250=0.41%, 500=1.51%, 750=0.90%, 1000=1.10%, 2000=29.02%
    lat (msec) : >=2000=67.02%
  cpu          : usr=0.02%, sys=0.13%, ctx=6821, majf=1324, minf=32897
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.3%, 16=99.4%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=5352/w=0/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16
file1: (groupid=3, jobs=1): err= 0: pid=12705: Wed Nov 11 02:15:14 2020
  write: io=8843.3MB, bw=10062KB/s, iops=39, runt=900002msec
    slat (usec): min=347, max=3623.1K, avg=25203.32, stdev=47989.86
    clat (usec): min=6, max=4836.7K, avg=378737.53, stdev=301135.99
     lat (usec): min=499, max=4871.1K, avg=403943.55, stdev=313617.83
    clat percentiles (msec):
     |  1.00th=[    8],  5.00th=[    9], 10.00th=[   27], 20.00th=[  120],
     | 30.00th=[  212], 40.00th=[  281], 50.00th=[  334], 60.00th=[  404],
     | 70.00th=[  486], 80.00th=[  578], 90.00th=[  742], 95.00th=[  914],
     | 99.00th=[ 1319], 99.50th=[ 1467], 99.90th=[ 2278], 99.95th=[ 2606],
     | 99.99th=[ 4752]
    bw (KB  /s): min=  117, max=87873, per=100.00%, avg=10450.16, stdev=7820.60
    lat (usec) : 10=0.01%, 20=0.01%, 50=0.01%, 750=0.01%
    lat (msec) : 2=0.02%, 4=0.01%, 10=6.52%, 20=0.56%, 50=6.24%
    lat (msec) : 100=4.80%, 250=17.14%, 500=36.46%, 750=18.53%, 1000=6.42%
    lat (msec) : 2000=3.14%, >=2000=0.12%
  cpu          : usr=0.17%, sys=2.40%, ctx=39444, majf=26, minf=11
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.2%, 16=99.6%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.1%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued    : total=r=0/w=35373/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
     latency   : target=0, window=0, percentile=100.00%, depth=16

Run status group 0 (all jobs):
  WRITE: io=110168KB, aggrb=122KB/s, minb=28KB/s, maxb=31KB/s, mint=901550msec, maxt=901615msec

Run status group 1 (all jobs):
   READ: io=20500KB, aggrb=22KB/s, minb=22KB/s, maxb=22KB/s, mint=900407msec, maxt=900407msec

Run status group 2 (all jobs):
   READ: io=1338.0MB, aggrb=1521KB/s, minb=1521KB/s, maxb=1521KB/s, mint=900377msec, maxt=900377msec

Run status group 3 (all jobs):
  WRITE: io=8843.3MB, aggrb=10061KB/s, minb=10061KB/s, maxb=10061KB/s, mint=900002msec, maxt=900002msec
make all
make[1]: Entering directory '/byte-unixbench/UnixBench'
make distr
make[2]: Entering directory '/byte-unixbench/UnixBench'
Checking distribution of files
./pgms  exists
./src  exists
./testdir  exists
./tmp  exists
./results  exists
make[2]: Leaving directory '/byte-unixbench/UnixBench'
make programs
make[2]: Entering directory '/byte-unixbench/UnixBench'
make[2]: Nothing to be done for 'programs'.
make[2]: Leaving directory '/byte-unixbench/UnixBench'
make[1]: Leaving directory '/byte-unixbench/UnixBench'

   #    #  #    #  #  #    #          #####   ######  #    #   ####   #    #
   #    #  ##   #  #   #  #           #    #  #       ##   #  #    #  #    #
   #    #  # #  #  #    ##            #####   #####   # #  #  #       ######
   #    #  #  # #  #    ##            #    #  #       #  # #  #       #    #
   #    #  #   ##  #   #  #           #    #  #       #   ##  #    #  #    #
    ####   #    #  #  #    #          #####   ######  #    #   ####   #    #

   Version 5.1.3                      Based on the Byte Magazine Unix Benchmark

   Multi-CPU version                  Version 5 revisions by Ian Smith,
                                      Sunnyvale, CA, USA
   January 13, 2011                   johantheghost at yahoo period com

------------------------------------------------------------------------------
   Use directories for:
      * File I/O tests (named fs***) = /byte-unixbench/UnixBench/tmp
      * Results                      = /byte-unixbench/UnixBench/results
------------------------------------------------------------------------------


1 x Dhrystone 2 using register variables  1 2 3 4 5 6 7