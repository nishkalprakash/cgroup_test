# This code is to scrape the docker changelogs
from pathlib import Path

from bs4 import BeautifulSoup
from requests import get

start_url = "https://github.com/docker/docker-ce/releases"

res = Path().cwd()/'res'


def get_page(i, start_url):
	html_file = res/f'{i}.html'
	if html_file.is_file():
		html = html_file.read_text()
	else:
		html = get(start_url).text
		html_file.write_text(html)

	soup = BeautifulSoup(html, features="lxml")

	data = ''
	releases = soup.find_all('div', 'markdown-body')
	for release in releases:
		data += release.get_text()+'\n\n'+'#'*100+"\n\n"

	start_url = soup.find_all('a', string='Next')[0].get('href')
	return i+1, data, start_url


i = 0
while i < 10:
	i, data, start_url = get_page(i, start_url)
	with open(res/'data.txt', 'a+') as f:
		f.write(data)
	print(f"Done for {i}.html\n")
