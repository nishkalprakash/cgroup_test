## This is to set the required cgroups on the docker

docker container update --cpuset-cpus 1-1 --cpu-quota 200000 --cpu-period 200000 attacker

docker container update --cpuset-cpus 0-0 --cpu-quota 200000 --cpu-period 200000 victim