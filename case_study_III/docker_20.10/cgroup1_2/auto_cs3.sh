#!/bin/bash

# echo "for linux"
# res="results"
# echo "for windows"
# res="results/win"
echo (){ 
    real_echo=/usr/bin/echo; $real_echo -n $(date) " "; $real_echo $@; 
}
if [ $(uname) == 'Linux' ]
then
    if [ -z $1 ]
    then
        res='results/ubuntu'
    else
        res="results/ubuntu/$1"
    fi
else
    res='results/win'
fi
tag="cs3"
##################################
echo "System Info"
bash system_info.sh $res
#################################
##################################
echo "Setup Images"
bash setup_images.sh $res $tag
#################################
# START For 1_io_dos

echo "START For 1_io_dos"
base="$res/1_io_dos"
mkdir -p $base
echo "Attacker Idle Loop on different core"
# for loop in {'idle_loop','sw_loop'}
for loop in 'sw_loop'
do
    att=$(docker run -d \
        --cpuset-cpus "1-1" \
        --cpu-period 200000 \
        --cpu-quota 200000 \
        attacker:$tag bash $loop.sh)

    for i in {'fio','ubench'}
    # for i in 'fio'
    do
        out="$base/$i"
        mkdir -p $out
        out="$out/different_core_$loop.txt"
        docker run -it \
            --cpuset-cpus "0-0" \
            --cpu-period 200000 \
            --cpu-quota 200000 \
            --mount src="$(pwd)/results",dst="/home/results",type=bind \
            victim2:$tag bash "$i"_tests.sh "$out"

        python extract_$i.py "$out"
    done

    docker stop $att
done

echo "Removing all the containers "

docker stop $(docker ps -q)
docker rm $(docker ps -aq)

echo "END For 1_io_dos"
# END For 1_io_dos

################################echo ""