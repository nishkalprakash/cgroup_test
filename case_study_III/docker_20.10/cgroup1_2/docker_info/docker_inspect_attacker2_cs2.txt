[
    {
        "Id": "sha256:8eb160b003d629fb97afaeaef9f7bb75f76b029d545531fc7f80738d10c6b6f8",
        "RepoTags": [
            "attacker2:cs2"
        ],
        "RepoDigests": [],
        "Parent": "",
        "Comment": "buildkit.dockerfile.v0",
        "Created": "2021-02-10T20:03:33.3596696Z",
        "Container": "",
        "ContainerConfig": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": null,
            "Cmd": null,
            "Image": "",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": null
        },
        "DockerVersion": "",
        "Author": "",
        "Config": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/bash"
            ],
            "Image": "",
            "Volumes": null,
            "WorkingDir": "/home",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": null
        },
        "Architecture": "amd64",
        "Os": "linux",
        "Size": 169819490,
        "VirtualSize": 169819490,
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/td9lqmaul065ru8f8h704jlgf/diff:/var/lib/docker/overlay2/jjen7y49fjg9u3up80tlytp57/diff:/var/lib/docker/overlay2/v260n681ubbcaz3z8ahwqg76c/diff:/var/lib/docker/overlay2/0a5a8cf41067456b4838e9bdeac436266bc3249b401ba51655e9c687d26f5ca4/diff:/var/lib/docker/overlay2/53a78d966e6ade872c4ce71d84b8458cbc95dce10178f1265c7916c791c29440/diff:/var/lib/docker/overlay2/b4b9a87521a9e7f9df0c0191fda6aa7352f874cf85612f5fb041cab1946da64d/diff:/var/lib/docker/overlay2/4114f2fcdf92325dfb7fdc1e6a36fd24cb41b223c57659caf14c13b1f642aedd/diff",
                "MergedDir": "/var/lib/docker/overlay2/41o1xnskmamlelzx7dwz45wg5/merged",
                "UpperDir": "/var/lib/docker/overlay2/41o1xnskmamlelzx7dwz45wg5/diff",
                "WorkDir": "/var/lib/docker/overlay2/41o1xnskmamlelzx7dwz45wg5/work"
            },
            "Name": "overlay2"
        },
        "RootFS": {
            "Type": "layers",
            "Layers": [
                "sha256:da2785b7bb163ff867008430c06b6c02d3ffc16fcee57ef38822861af85989ea",
                "sha256:bbc674332e2e42ff92e896664df969c201a969b9ec096c5905aa725148d1c4f5",
                "sha256:5b7dc8292d9bd9c2bec6b0ec05259f697171360b8b52fe9d61aaa42226d911b2",
                "sha256:1a1a19626b20bd2320ad590b025f8ced47bab9d667bc256e378ff6df3674e2c4",
                "sha256:5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef",
                "sha256:80949a3d29fb1cee139b70e93b9620fcf25c8496d2623bdfcbe6e0a56074a595",
                "sha256:8811e9b5f79aa8143a3977894c9804c4dd65b1079c6f5abe0fe1553290bc485d",
                "sha256:b71c970040ee0baa2ace794d3faa41179e5c7e037813bb7da0c4ab6bd11ae830"
            ]
        },
        "Metadata": {
            "LastTagTime": "2021-02-11T05:54:01.9123869Z"
        }
    }
]
