#!/bin/bash

# echo "for linux"
# res="results"
# echo "for windows"
# res="results/win"
echo (){ 
    real_echo=/usr/bin/echo; $real_echo -n $(date) " "; $real_echo $@; 
}
if [ $(uname) == 'Linux' ]
then
    if [ -z $1 ]
    then
        res='results/ubuntu'
    else
        res="results/ubuntu/$1"
    fi
else
    res='results/win'
fi
mkdir -p $res
tag="cs4"
##################################
echo "System Info"
bash system_info.sh $res
#################################
##################################
echo "Setup Images"
bash setup_images.sh $res $tag
#################################
# START For 1_cpu_workload_amp

echo "START For 1_cpu_workload_amp"
base="$res/1_cpu_workload_amp"
mkdir -p $base
echo "Attacker running lsmod"
att=$(docker run -d \
        --cpuset-cpus "0-0" \
        --cpu-period 200000 \
        --cpu-quota 200000 \
        attacker:$tag bash lsmod_loop.sh)

echo "Host running pidstat to measure workload impact"
pidstat_out="$base/pidstat_results.txt"

bash pid_stat.sh "$pidstat_out"

# docker stop $att

echo "Removing all the containers "

docker stop $(docker ps -q)
docker rm $(docker ps -aq)

echo "END For 1_cpu_workload_amp"
# END For 1_cpu_workload_amp
################################
# START For 2_dos

echo "START For 2_dos"
base="$res/2_dos"
mkdir -p $base

out="$base/sysbench"
mkdir -p $out
###########
echo "Victim running sysbench with attacker running idle loop"
att=$(docker run -d \
        --cpuset-cpus "0-0" \
        --cpu-period 200000 \
        --cpu-quota 200000 \
        attacker:$tag bash idle_loop.sh)

sys_out="$out/baseline_with_idle_loop_diff_core.txt"
docker run -it \
    --cpuset-cpus "2-2" \
    --cpu-period 200000 \
    --cpu-quota 200000 \
    --mount src="$(pwd)/results",dst="/home/results",type=bind \
    victim:$tag bash sysbench_test.sh "$sys_out"

python extract_sysbench.py "$sys_out"

docker stop $att

###########
echo "Attacker running lsmod"
att=$(docker run -d \
        --cpuset-cpus "0-0" \
        --cpu-period 200000 \
        --cpu-quota 200000 \
        attacker:$tag bash lsmod_loop.sh)
###########
for loop in {'2-2','0-0'}
do
    echo "Victim running sysbench on core $loop"

    sys_out="$out/sysbench_$loop.txt"
    docker run -it \
        --cpuset-cpus "$loop" \
        --cpu-period 200000 \
        --cpu-quota 200000 \
        --mount src="$(pwd)/results",dst="/home/results",type=bind \
        victim:$tag bash sysbench_test.sh "$sys_out"

    python extract_sysbench.py "$sys_out"
done

docker stop $att

echo "Removing all the containers "

docker stop $(docker ps -q)
docker rm $(docker ps -aq)

echo "END For 2_dos"
# END For 2_dos

################################