sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.6753s
    total number of events:              10000
    total time taken by event execution: 13.6740
    per-request statistics:
         min:                                  1.36ms
         avg:                                  1.37ms
         max:                                  1.60ms
         approx.  95 percentile:               1.37ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.6740/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2160087.24 ops/sec)

102400.00 MB transferred (2109.46 MB/sec)


Test execution summary:
    total time:                          48.5432s
    total number of events:              104857600
    total time taken by event execution: 38.2666
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.08ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.2666/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.6113s
    total number of events:              10000
    total time taken by event execution: 13.6100
    per-request statistics:
         min:                                  1.36ms
         avg:                                  1.36ms
         max:                                  1.61ms
         approx.  95 percentile:               1.37ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.6100/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2127835.90 ops/sec)

102400.00 MB transferred (2077.96 MB/sec)


Test execution summary:
    total time:                          49.2790s
    total number of events:              104857600
    total time taken by event execution: 38.8397
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.09ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.8397/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.7783s
    total number of events:              10000
    total time taken by event execution: 13.7770
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.38ms
         max:                                  1.69ms
         approx.  95 percentile:               1.56ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.7770/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2169257.21 ops/sec)

102400.00 MB transferred (2118.42 MB/sec)


Test execution summary:
    total time:                          48.3380s
    total number of events:              104857600
    total time taken by event execution: 38.0980
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  2.27ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.0980/0.00

