sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.6646s
    total number of events:              10000
    total time taken by event execution: 13.6631
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.37ms
         max:                                 23.52ms
         approx.  95 percentile:               1.39ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.6631/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2137573.04 ops/sec)

102400.00 MB transferred (2087.47 MB/sec)


Test execution summary:
    total time:                          49.0545s
    total number of events:              104857600
    total time taken by event execution: 38.6633
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.33ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.6633/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.9607s
    total number of events:              10000
    total time taken by event execution: 13.9584
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.40ms
         max:                                  1.95ms
         approx.  95 percentile:               1.56ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.9584/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2093930.39 ops/sec)

102400.00 MB transferred (2044.85 MB/sec)


Test execution summary:
    total time:                          50.0769s
    total number of events:              104857600
    total time taken by event execution: 39.4648
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.34ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   39.4648/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.9049s
    total number of events:              10000
    total time taken by event execution: 13.9027
    per-request statistics:
         min:                                  1.36ms
         avg:                                  1.39ms
         max:                                  1.85ms
         approx.  95 percentile:               1.51ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.9027/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2138806.88 ops/sec)

102400.00 MB transferred (2088.68 MB/sec)


Test execution summary:
    total time:                          49.0262s
    total number of events:              104857600
    total time taken by event execution: 38.6423
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.32ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.6423/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.5985s
    total number of events:              10000
    total time taken by event execution: 13.5972
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.36ms
         max:                                  1.67ms
         approx.  95 percentile:               1.37ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.5972/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2169536.56 ops/sec)

102400.00 MB transferred (2118.69 MB/sec)


Test execution summary:
    total time:                          48.3318s
    total number of events:              104857600
    total time taken by event execution: 38.0928
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.09ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.0928/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.6596s
    total number of events:              10000
    total time taken by event execution: 13.6583
    per-request statistics:
         min:                                  1.36ms
         avg:                                  1.37ms
         max:                                  1.69ms
         approx.  95 percentile:               1.39ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.6583/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2119663.32 ops/sec)

102400.00 MB transferred (2069.98 MB/sec)


Test execution summary:
    total time:                          49.4690s
    total number of events:              104857600
    total time taken by event execution: 38.9869
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  1.36ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.9869/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing CPU performance benchmark

Threads started!
Done.

Maximum prime number checked in CPU test: 10000


Test execution summary:
    total time:                          13.7279s
    total number of events:              10000
    total time taken by event execution: 13.7263
    per-request statistics:
         min:                                  1.35ms
         avg:                                  1.37ms
         max:                                  1.73ms
         approx.  95 percentile:               1.45ms

Threads fairness:
    events (avg/stddev):           10000.0000/0.00
    execution time (avg/stddev):   13.7263/0.00

sysbench 0.4.12:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 1

Doing memory operations speed test
Memory block size: 1K

Memory transfer size: 102400M

Memory operations type: write
Memory scope type: global
Threads started!
Done.

Operations performed: 104857600 (2169421.19 ops/sec)

102400.00 MB transferred (2118.58 MB/sec)


Test execution summary:
    total time:                          48.3344s
    total number of events:              104857600
    total time taken by event execution: 38.0969
    per-request statistics:
         min:                                  0.00ms
         avg:                                  0.00ms
         max:                                  0.08ms
         approx.  95 percentile:               0.00ms

Threads fairness:
    events (avg/stddev):           104857600.0000/0.00
    execution time (avg/stddev):   38.0969/0.00

