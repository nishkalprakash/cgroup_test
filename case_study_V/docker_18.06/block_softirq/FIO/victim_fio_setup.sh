## To copy fio_tests files to victim docker

docker cp fio_tests victim:/fio_tests/


## To install fio into victim

docker start -i victim

## Inside victim container

apt-get update

apt-get install fio

