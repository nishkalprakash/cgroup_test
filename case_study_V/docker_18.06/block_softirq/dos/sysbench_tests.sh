# sysbench --test=cpu run && \
# sysbench --test=memory run && \
# sysbench --test=fileio --file-test-mode=seqrd run && \
# sysbench --test=fileio --file-test-mode=seqwr run

sysbench --test=cpu run && \
sysbench --test=memory run && \
sysbench --test=fileio --file-test-mode=seqrd prepare && \
sysbench --test=fileio --file-test-mode=seqrd run && \
sysbench --test=fileio --file-test-mode=seqwr prepare && \
sysbench --test=fileio --file-test-mode=seqwr run && \
sysbench --test=fileio --file-test-mode=seqrd cleanup && \
sysbench --test=fileio --file-test-mode=seqwr cleanup 
