#!/bin/bash 
default="results"

mkdir -p $default

if [ -z "$1" ]
    then 
    results_file= "$default/fio_results.txt"
else
    results_file="$1"
    mkdir -p $(dirname "${results_file}")
fi

for i in {1..10}
do
    fio fio_tests/fio-seq-read.fio \
        fio_tests/fio-seq-write.fio  >> "$results_file" 2>&1
    echo "FIO Test - PASS $i Success"
done

unset ubase
unset results_file