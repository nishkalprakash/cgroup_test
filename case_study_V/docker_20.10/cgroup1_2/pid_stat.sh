

default="results"

mkdir -p $default

if [ -z "$1" ]
then 
    results_file="$default/pid_stat_results.txt"
else
    results_file="$1"
    mkdir -p "$(dirname "${results_file}")"
fi

for i in {1..3}
do
    echo $results_file
    pidstat 1 150 -C 'kworker|softirq|kswapd|jbd2' >> "$results_file" 2>&1
    echo "pid_stat.sh - PASS $i Success"

done
