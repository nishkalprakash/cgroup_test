[
    {
        "Id": "sha256:49b4492c69a3e66b8c69e80f482115e322dd0cc0b87312ba15e32692d217fcbe",
        "RepoTags": [
            "victim:cs5"
        ],
        "RepoDigests": [],
        "Parent": "sha256:03ecc9b2f965c16a82267c30bbbed627647d22f8c99914657f405fd17e5c5530",
        "Comment": "",
        "Created": "2021-02-28T16:57:29.430810276Z",
        "Container": "",
        "ContainerConfig": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/sh",
                "-c",
                "#(nop) COPY dir:614dc775abd0e07478c8adda9abe2d7aadd4aff07113fc62f755644c6de8dfe4 in /home "
            ],
            "Image": "sha256:03ecc9b2f965c16a82267c30bbbed627647d22f8c99914657f405fd17e5c5530",
            "Volumes": null,
            "WorkingDir": "/home",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": null
        },
        "DockerVersion": "20.10.4",
        "Author": "",
        "Config": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/bash"
            ],
            "Image": "sha256:03ecc9b2f965c16a82267c30bbbed627647d22f8c99914657f405fd17e5c5530",
            "Volumes": null,
            "WorkingDir": "/home",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": null
        },
        "Architecture": "amd64",
        "Os": "linux",
        "Size": 170963425,
        "VirtualSize": 170963425,
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/e378256f1d4f1a6eb5390fd11afece012fa80debc9074e08c5476569bb4c1829/diff:/var/lib/docker/overlay2/9395b669698d81626a9a218e7ac9bf5438b960f0602b163a9aa2b8eef28511d0/diff:/var/lib/docker/overlay2/89a936cd1adbed7f8b576781ca74af003df88ef9c2cd5e74c69b4f6bece25557/diff:/var/lib/docker/overlay2/33ce4553898e3ca85b95008d36b3223503963466a5a0a1b63bfa07e52fc46c12/diff:/var/lib/docker/overlay2/d5b634617ada5a03c19cb99531fb2d69e4bb3c727cf151489016ff9f2fff1206/diff:/var/lib/docker/overlay2/4bf5c683d5d3203ea4e55ceb6dad4c61590a815c87d779507a47127dbd54543b/diff",
                "MergedDir": "/var/lib/docker/overlay2/e68b536303404934056188acab05bf4a04f0624f6c9dff039609c90dd3a3d0b5/merged",
                "UpperDir": "/var/lib/docker/overlay2/e68b536303404934056188acab05bf4a04f0624f6c9dff039609c90dd3a3d0b5/diff",
                "WorkDir": "/var/lib/docker/overlay2/e68b536303404934056188acab05bf4a04f0624f6c9dff039609c90dd3a3d0b5/work"
            },
            "Name": "overlay2"
        },
        "RootFS": {
            "Type": "layers",
            "Layers": [
                "sha256:935c56d8b3f96d6587f3640e491767688b790c458a01fef327188abcbbafdc9a",
                "sha256:697949baa6589708187ff25708ee6c7cf2c6657399552a019598b1da7f617acf",
                "sha256:e6feec0db89a854a83e704823cdd269ad82625cb57ecc06b4ede6095948f66db",
                "sha256:5276d2b930fc59425e6cf44315e0ca0de5948865d615de79e34d5ff9bf3a9b96",
                "sha256:6a5d3297345271fc3d86c39f859379de157eaac883e395eb13db5ff6b711a4c2",
                "sha256:9b31dafa11235c41f33df5a9ea82475e59ffb0083def715e24b470f96924242c",
                "sha256:0274a72dc503ce743109593b1ba32868cacea6609466f0e0402265a64ba3e373"
            ]
        },
        "Metadata": {
            "LastTagTime": "2021-03-03T00:37:16.943021612+05:30"
        }
    }
]
