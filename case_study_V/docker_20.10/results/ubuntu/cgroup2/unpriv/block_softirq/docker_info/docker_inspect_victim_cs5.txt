[
    {
        "Id": "sha256:c4ec68f53f5784af3acc0fc21258d1d0f52f3369c589482ba6505b5afbbda57f",
        "RepoTags": [
            "victim:cs5"
        ],
        "RepoDigests": [],
        "Parent": "sha256:9270928d7655390197c78655871168518a8a38ab1cf6a56b0bb6e928d9112f76",
        "Comment": "",
        "Created": "2021-02-20T15:50:04.000258895Z",
        "Container": "",
        "ContainerConfig": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/sh",
                "-c",
                "#(nop) COPY dir:614dc775abd0e07478c8adda9abe2d7aadd4aff07113fc62f755644c6de8dfe4 in /home "
            ],
            "Image": "sha256:9270928d7655390197c78655871168518a8a38ab1cf6a56b0bb6e928d9112f76",
            "Volumes": null,
            "WorkingDir": "/home",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": null
        },
        "DockerVersion": "20.10.3",
        "Author": "",
        "Config": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/bash"
            ],
            "Image": "sha256:9270928d7655390197c78655871168518a8a38ab1cf6a56b0bb6e928d9112f76",
            "Volumes": null,
            "WorkingDir": "/home",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": null
        },
        "Architecture": "amd64",
        "Os": "linux",
        "Size": 170887269,
        "VirtualSize": 170887269,
        "GraphDriver": {
            "Data": {
                "LowerDir": "/home/darkskull/.local/share/docker/overlay2/614980be88253f8b884c499467d70b06cb08834ff7eeab2f6135def6e5d86222/diff:/home/darkskull/.local/share/docker/overlay2/8dab19267bf62b9e5da868762e26c925504c1aadd98046aadf1454b928aa748c/diff:/home/darkskull/.local/share/docker/overlay2/e30e2b8b2b33e3be2f5a6b57dd1ab11269618da3625321e7a2a1d34d9002a3ca/diff:/home/darkskull/.local/share/docker/overlay2/0d3c41ea413abc0437fa102c33a63ba132085949fc364b20d6071b65ff7de064/diff:/home/darkskull/.local/share/docker/overlay2/2ab1a970fa25e9bb5454bb975d0bdb8dd4644e95aa9d95c9d2ab4df725db292d/diff:/home/darkskull/.local/share/docker/overlay2/80af976d39241d9abff5174b63f7d44ee5a892be7eed04bdcbae1cee294018f9/diff",
                "MergedDir": "/home/darkskull/.local/share/docker/overlay2/ba2306ed361d5c93048c94e5b867c538ced46e521c16a3f2d45f98394f3e4a5d/merged",
                "UpperDir": "/home/darkskull/.local/share/docker/overlay2/ba2306ed361d5c93048c94e5b867c538ced46e521c16a3f2d45f98394f3e4a5d/diff",
                "WorkDir": "/home/darkskull/.local/share/docker/overlay2/ba2306ed361d5c93048c94e5b867c538ced46e521c16a3f2d45f98394f3e4a5d/work"
            },
            "Name": "overlay2"
        },
        "RootFS": {
            "Type": "layers",
            "Layers": [
                "sha256:935c56d8b3f96d6587f3640e491767688b790c458a01fef327188abcbbafdc9a",
                "sha256:697949baa6589708187ff25708ee6c7cf2c6657399552a019598b1da7f617acf",
                "sha256:e6feec0db89a854a83e704823cdd269ad82625cb57ecc06b4ede6095948f66db",
                "sha256:5276d2b930fc59425e6cf44315e0ca0de5948865d615de79e34d5ff9bf3a9b96",
                "sha256:20c6d91a01a8a2015aedf6fd377211805d416a127a7d5e9e5d2be157cc181802",
                "sha256:c7fa6f0e8a24ee272fe65826bebe8d32185e23f3643c0e1286fb80437a096b11",
                "sha256:8dc0b472dce51ed6b4f836bfa07a91337c07565faed05fb3d6d34d4fcee82c0d"
            ]
        },
        "Metadata": {
            "LastTagTime": "2021-02-20T21:20:04.51154865+05:30"
        }
    }
]
