## Run this file with arg1=InputFile.txt and it will output as InputFile.csv
import re
import sys

# with open("fio.txt") as f:
with open(sys.argv[1]) as f:
    text = f.read()

# run regular expressions to extrated the required data
head_main = "IO test,io (KB),aggrb (KB/s),minb (KB/s),maxb (KB/s)".split(',')
labels = re.findall(r"file1: \(g=\d\): rw=(.*?),", text)
key = {
    "randread": "fio-rand-read",
    "randwrite": "fio-rand-write",
    "read": "fio-seq-read",
    "write": "fio-seq-write",
}
data = re.findall(r"Run status group .*\n(.*)", text)
i = 0
results = []
for t in data:
    z = []
    if i % 4 == 0:
        if i:
            results.append([])
        head=head_main[:]
        head[0]=f"PASS_{(i//4)+1}: "+head[0]
        results.append(head)
    for item in t.split(",")[:-2]:
        string = re.findall(r"\d+.*", item)[0].strip("KB/s")
        try:
            z.append(float(string))
        except ValueError:
            z.append(float(string[:-1]) * 1024)

    # labels = zip(labels, z)
    results.append([key[labels[i]]] +  z)
        # print(i)
    # print(i)
    i += 1

    # tl.append(z)
l=len(results)
n=l//5
results.append([])
head=head_main[:]
head[0]="AVG: "+head[0]
results.append(head)
for i in range(4):
    ll=[key[labels[i]]]
    for k in range(4):
        s=0
        for j in range(i+1,l,6):
            s+=results[j][k+1]
        ll.append(s/n)
    results.append(ll)
results_str=list(map(lambda x: "\t".join(map(lambda y: f'{y:0.2f}' if type(y)==float else y,x)),results))
print("\n".join(results_str[-5:]))
try:
    out = sys.argv[1].replace("txt", "tsv")
except IndexError:
    print("Please enter output file")
    # exit()
    out="fio.tsv"
with open(out, "w+") as f:
    # f.write(head + "\n")
    f.write("\n".join(results_str))
    # f.write
