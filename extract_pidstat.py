## Run this file with arg1=/path/to/file/InputFile.txt and it will output as /path/to/file/InputFile.csv
#%% INIT
import re
import sys

# with open("pidstat_results.txt") as f:
with open(sys.argv[1]) as f:
    text = f.read()

#%% extracting the info
# run regular expressions to extrated the required data
avg=re.findall(r'Aver.*',text)
avg=[re.sub(r'\s+',';',i).split(";") for i in avg]
avg_dict={}
for i in avg:
    try:
        avg_dict[i[-1]].append(i)
    except:
        avg_dict[i[-1]]=[i]
final_avg=[]
for i in avg_dict:
    sum_dict={}
    for j in avg_dict[i]:
        n=len(j)
        for k in range(n):
            try:
                sum_dict[k]=float(j[k])+float(sum_dict[k])
            except:
                try:
                    sum_dict[k]=float(j[k])
                except:
                    sum_dict[k]=j[k]
    l=len(avg_dict[i])
    for k in range(n):
        try:
            sum_dict[k]=f"{sum_dict[k]/l:0.2f}"
        except:
            pass
    final_avg.append(list(sum_dict.values()))
#%% Ouput the result
final_avg.sort(key=lambda x:x[-1])

neg_match=[
    "power",
    "flush"
]
results=["\t".join(i[1:-2]+i[-1:]) for i in final_avg if all(x not in i[-1] for x in neg_match)]
try:
    out = sys.argv[1].replace("txt", "tsv")
except IndexError:
    print("Please enter output file")
    out = "pidstat_results.tsv"
    # exit()
with open(out, "w+") as f:
    # f.write(head + "\n")
    f.write("\n".join(results))
    # f.write

# %%
