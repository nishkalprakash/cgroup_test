## Run this file with arg1=InputFile.txt and it will output as InputFile.csv
import re
import sys

# with open("sysbench.txt") as f:
with open(sys.argv[1]) as f:
    text = f.read()


def test_type():
    ## This fucntion returns the file name as str
    if "\\" in sys.argv[1]: 
        return sys.argv[1].split("\\")[-1].split(".")[0]
    if "/" in sys.argv[1]: 
        return sys.argv[1].split("/")[-1].split(".")[0]
    return sys.argv[1].split(".")[0]


def cpu_list():
    ## This fucntion returns the list of CPU stats or returns 0 if not found

    cpu = list(
        map(
            lambda x: f"{10000/float(x):0.2f}",
            re.findall(
                r"events \(avg/stddev\):\s+10000\.0000/0\.00\n.*?\(avg/stddev\):\s+(.*?)/0\.00",
                text,
            ),
        )
    )
    if cpu:
        return ["CPU (Events / s)"] + cpu
    return 0


def memory_list():
    memory = re.findall(r"102400\.00 MB transferred \((.*?) MB\/sec\)", text)
    if memory:
        return ["Memory (MB / s)"] + memory
    else:
        return 0


def seqrd_list():
    seqrd = re.findall(
        r"Read 2Gb  Written 0b  Total transferred 2Gb  \((.*?)b/sec\)", text
    )
    # print(seqrd)
    if seqrd:
        seqrd = list(
            map(
                lambda x: f"{float(x[:-1])*1024/8 if x[-1]=='G' else float(x[:-1])/8:.2f}",
                seqrd,
            )
        )
        # for s in seqrd:
        #     if s[-1]=='M':
        return ["I/O Read (MB /s)"] + seqrd
    else:
        return 0


def seqwt_list():
    seqwt = re.findall(
        r"Read 0b  Written 2Gb  Total transferred 2Gb  \((.*?)b/sec\)", text
    )
    # print(seqwt)
    if seqwt:
        seqwt = list(
            map(
                lambda x: f"{float(x[:-1])*1024/8 if x[-1]=='G' else float(x[:-1])/8:.2f}",
                seqwt,
            )
        )
        # for s in seqrd:
        #     if s[-1]=='M':
        return ["I/O Write (MB /s)"] + seqwt
    else:
        return 0


# run regular expressions to extrated the required data

results = [test_type()]

for i in [cpu_list(), memory_list(), seqrd_list(), seqwt_list()]:
    if i:
        i.append(f"{sum(map(float,i[1:]))/len(i[1:]):0.2f}")
        results.append(i)

results[0]=[results[0]]+[f'pass_{i+1}' for i in range(len(results[1][1:-1]))]+['Avg']
# results += list(
#     map(
#         lambda x: ",".join(x),
#         zip(test_type(n), ),
#     )
# )
# result = [[results[j][i] for j in range(len(results))] for i in range(len(results[0]))]
result=zip(*results)
# print(result)

try:
    out = sys.argv[1].replace("txt", "tsv")
except IndexError:
    print("Please enter output file")
    out = "sysbench.tsv"
    # exit()
with open(out, "w+") as f:
    # f.write(head + "\n")
    f.write("\n".join(map("\t".join,result)))
    # f.write
