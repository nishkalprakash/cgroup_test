## Run this file with arg1=InputFile.txt and it will output as InputFile.csv
import re
import sys

# with open("ubench.txt") as f:
with open(sys.argv[1]) as f:
    text = f.read()

# run regular expressions to extrated the required data
head_main = "test,name,baseline,result,index".split(',')

labels = re.findall(
    r"System Benchmarks Partial Index (.*?)System Benchmarks Index Score",
    text,
    re.DOTALL,
)
overall = re.findall(r"System Benchmarks Index Score \(Partial Only\)\s+(.*?)\n", text)
# print(overall)
# x = labels[0]

key = {
    "Shell Scripts (8 concurrent)": "Shell Scripts 8",
    "Execl Throughput": "Execl",
    "Double-Precision Whetstone": "Whetstone",
    "Process Creation": "Process Creation",
    "System Call Overhead": "Sys Call Overhead",
    "File Copy 4096 bufsize 8000 maxblocks": "File Copy 4096/8000",
    "Pipe-based Context Switching": "Pipe Ctx Sw",
    "Pipe Throughput": "Pipe Throughput",
    "Dhrystone 2 using register variables": "Dhrystone",
    "Shell Scripts (1 concurrent)": "Shell Scripts 1",
    "File Copy 1024 bufsize 2000 maxblocks": "File Copy 1024/2000",
    "File Copy 256 bufsize 500 maxblocks": "File Copy 256/500",
}
i = 0
results = []
for x in labels:
    if i:
        results.append([])
    head=head_main[:]
    head[0]=f"PASS_{(i//4)+1}: "+head[0]
    results.append(head)
    z = re.sub("  +", ",", x.strip()).split("\n")[1:-1]
    for b in range(len(z)):
        a = z[b].split(",")
        z[b] = [a[0], key[a[0]]] + a[1:]
    z.append(f"System Benchmarks Index Score,Overall,,,{overall[i]}".split(','))
    results.extend(z)
    # print(i)
    # print(i)
    i += 1

    # tl.append(z)

l=len(results) 
n=i # number of passes
t=(l//(i+1))+1 # number of tests per pass + 1
results.append([])
head=head_main[:]
head[0]="AVG: "+head[0]
results.append(head)
for i in range(1,t):
    # i is for iterating over passes
    ll=results[i][:3]
    for k in range(3,5):
        # k is for iterating over columns
        s=0
        for j in range(i,l,t+1):
            # j is for iterating over rows
            if results[j][k]:
                s+=float(results[j][k])
        if s:
            ll.append(s/n)
        else:
            ll.append('')
    results.append(ll)
results_str=list(map(lambda x: "\t".join(map(lambda y: f'{y:0.2f}' if type(y)==float else y,x)),results))
print("\n".join(results_str[-t:]))
try:
    out = sys.argv[1].replace("txt", "tsv")
except IndexError:
    print("Please enter output file")
    out = "ubench.tsv"
    # exit()
with open(out, "w+") as f:
    # f.write(head + "\n")
    f.write("\n".join(results_str))
    # f.write
